#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include<sys/wait.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include "functions.h"
int exit_flag;
int requests_count;
int log_flag;//flag for sigint or sigkill


void sigchld_f(int sig)
{
  if(exit_flag!=1)
  {
    pid_t pid;
    int   status;
    pid = wait(NULL);
    printf("a child terminated-> %d\n",pid );
  }
}

void sigint_f(int sig)
{
  printf("SIGINT detected by parent\n");
  log_flag = 1;
}
void sigquit_f(int sig)
{
  printf("SIGQUIT  detected by parent\n");
  log_flag = 1;
}

int main(int argc,char* argv[])
{
  //signal(SIGINT,sigint_f);
  signal(SIGCHLD,sigchld_f);
  signal(SIGQUIT,sigquit_f);
  exit_flag = 0; //flag to know if an exit order came
  requests_count = 0;
  log_flag = 0;

  //Check the num of arguments
  if(argc!=11)
  {
    printf("Wrong arguments.Please try again\n");
    return 0;
  }
  //take the arguments
  int numWorkers  = atoi(argv[2]);
  int bufferSize  = atoi(argv[4]);
  const char* serverIP = argv[6];
  int serverPort = atoi(argv[8]);
  const char*  input_dir = (argv[10]);
  //printf("numWorkers->%d\nbuffersize->%d\ndir->%s\nserverIP->%s\nserverPort->%d\n",numWorkers,bufferSize,input_dir,serverIP,serverPort );

  //  opening parent directory
  DIR *parent_dir;
  parent_dir = opendir(input_dir);
  if(parent_dir==NULL)
  {
    printf("ERROR at opening parent_dir\n");
    return 0;
  }


  country_parent_list* parent_list = malloc(sizeof(country_parent_list));
  country_parent_node* ptr = parent_list->head;


  //read subdirectories
  struct dirent* sub_dir;
  int subdirectories_counter = 0;
  while ((sub_dir = readdir(parent_dir)))
  {
    if (!strcmp (sub_dir->d_name, ".") || !strcmp (sub_dir->d_name, ".."))
            continue;
    //printf("--------->%s\n",sub_dir->d_name);

    //add to list
    if(subdirectories_counter==0)//add to head
    {
      parent_list->head = malloc(sizeof(country_parent_node));
      strcpy(parent_list->head->country,sub_dir->d_name);
      parent_list->head->country_num = subdirectories_counter;
      parent_list->head->next = NULL;
      ptr = parent_list->head;
    }
    else
    {
      ptr->next = malloc(sizeof(country_parent_node));
      ptr = ptr->next;
      strcpy(ptr->country,sub_dir->d_name);
      ptr->country_num = subdirectories_counter;
      ptr->next = NULL;
    }

    subdirectories_counter++;

  }
  if(subdirectories_counter<numWorkers)//if proccesses > subdirectories there is no need to make that many
  numWorkers = subdirectories_counter;

  //create pipe ZERO FOR WRITTING AND ONE FOR READING
  //ex.pipe51 is pipe for worker num 5 and pipe for reading
  int w_r;
  char pipe_name[20];
  for(int id = 0;id<numWorkers;id++)
  {
    w_r = 0;
    snprintf(pipe_name,sizeof(pipe_name),"/tmp/pipe%d%d",id,w_r);
    mkfifo(pipe_name,0666);
    //printf("%s\n",pipe_name);
    w_r = 1;
    snprintf(pipe_name,sizeof(pipe_name),"/tmp/pipe%d%d",id,w_r);
    mkfifo(pipe_name,0666);
    //printf("%s\n",pipe_name);

  }

  int readfd, writefd;

  //create the workers
  //-----------------------------------------------------------------------------------------------------------
  pid_t pid;
  for(int i=0; i<numWorkers; i++) // loop will run n times
  {
      pid = fork();
      if(pid == 0)//if it is a worker
      {
        char exec_worker_id[10];
        char exec_buffersize[10];
        char exec_parent_dir[20];
        char exec_server_ip[100];
        char exec_server_port[100];

        //strcpy (exec_parent_dir,input_dir);
        snprintf(exec_worker_id,sizeof(exec_worker_id),"%d",i);
        snprintf(exec_parent_dir,sizeof(exec_parent_dir),"%s",input_dir);
        snprintf(exec_buffersize,sizeof(exec_buffersize),"%d",bufferSize);
        snprintf(exec_server_ip,sizeof(exec_server_ip),"%s",serverIP);
        snprintf(exec_server_port,sizeof(exec_server_port),"%d",serverPort);


        if(execlp("./Worker",exec_parent_dir,exec_buffersize,exec_worker_id,exec_server_ip,exec_server_port ,(int *)0  ) == -1)
        {
                printf("exec not working \n");
                exit(1);
        }
      }

  }

  //create array to keep the fd.0 for WRITTING 1 for reading
  int **fd_array;
  fd_array = malloc(numWorkers*sizeof(int*)); //create an array of pointers
  for(int i=0;i<numWorkers;i++)
  {
    fd_array[i] = malloc(2*sizeof(int)); //allocate mem for the integers
  }
  for(int i = 0; i < numWorkers;i++)
  {
    //open write pipe
    snprintf(pipe_name,sizeof(pipe_name),"/tmp/pipe%d0",i);
    if((writefd = open(pipe_name, O_WRONLY))<0)
    printf("error opening w_pipe \n" );

    //open read_pipe
    snprintf(pipe_name,sizeof(pipe_name),"/tmp/pipe%d1",i);
    if((readfd = open(pipe_name, O_RDONLY))<0)
    printf("error opening r_pipe \n");

    //put fd in an array
    fd_array[i][0] = writefd;
    fd_array[i][1] = readfd;
    // printf("fd array[%d]0 is %d\n",i,fd_array[i][0]);
    // printf("fd array[%d]1 is %d\n",i,fd_array[i][1]);

  }


  //calculate subdir to give to the Workers.......................
  int each_w_takes = subdirectories_counter/numWorkers;
  int mod = subdirectories_counter%numWorkers;
  int l = subdirectories_counter*2;
  char send_dir[l];//max num of dir that might be needed * char to seperate them
  char temp[20];

  int tmp_num;
  int sub_dir_give = 0;
  int first,last;
  for(int w = 0; w < numWorkers;w++)
  {
    //send to the worker the first and the last subdir he has to read
    tmp_num = each_w_takes;
    first = sub_dir_give + 1;
    sub_dir_give = sub_dir_give + 1;
    if(w < mod)//if its onw of the first workers that will take extra dirs
    {
      last = sub_dir_give + each_w_takes ;
      sub_dir_give = sub_dir_give + each_w_takes ;
    }
    else
    {
      last = sub_dir_give + each_w_takes -1;
      sub_dir_give = sub_dir_give + each_w_takes -1 ;
    }
    snprintf(temp,sizeof(temp),"%d %d",first,last);
    insert_dirs_to_list(parent_list,first,last,w);
    write(fd_array[w][0],temp, 20);
  }

  //create aary with pid_t
  int* pid_array = malloc(numWorkers* sizeof(int));
  for(int i =0; i< numWorkers;i++)
  {
    read(fd_array[i][1],temp,10);
    pid_array[i] = atoi(temp);
    //printf("----->%d\n",pid_array[i]);
  }

  //at project 2
  //get stats
  //read from terminal
  //but we dont need that

  //this is the parent
  for(int i=0; i<numWorkers; i++) // loop in order to wait for the children to finish
  wait(NULL);


  if(log_flag == 1)
  {
    //send kill to workers
    for(int i = 0; i < numWorkers;i++)//send exit option to all workers
    {
      kill(pid_array[i],SIGKILL);
      make_parent_log(parent_list,requests_count);
    }
  }
  //make log file
  //free the list
  country_parent_node* free_ptr = parent_list->head;
  country_parent_node* tmp_ptr = free_ptr;
  while(free_ptr != NULL)
  {
    free_ptr = free_ptr->next;
    free(tmp_ptr);
    tmp_ptr = free_ptr;
  }
  for(int i=0;i<numWorkers;i++)
  {
    free(fd_array[i]);
  }
  free(fd_array);
  free(pid_array);
  free(parent_list);
  closedir(parent_dir);
  return 0;
}
