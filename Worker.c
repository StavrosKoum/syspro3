#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include<sys/wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <signal.h>
#include "functions.h"
#include "functions2.h"
#include <sys/wait.h> /* sockets */
#include <sys/types.h> /* sockets */
#include <sys/socket.h> /* sockets */
#include <netinet/in.h> /* internet sockets */
#include <netdb.h> /* gethostbyaddr */
int signal_num;
int requests_count;
country_list_node* countries;

void sigint_handler(int sig)
{
  signal_num = sig;
  printf("I received the signal ->%d\n",signal_num );
  make_logfile(countries,requests_count);

}

void sigquit_handler(int sig)
{
  signal_num = sig;
  printf("I received the signal ->%d\n",signal_num );
  make_logfile(countries,requests_count);
}


int main(int argc,char* argv[])
{

  //signal(SIGINT,sigint_handler);
  signal(SIGQUIT,sigquit_handler);
  // printf("alalalala\n" );
  //take the arguments
  const char*  input_dir = (argv[0]);
  int bufferSize = atoi(argv[1]);
  int worker_id = atoi(argv[2]);//worker id to know which pipe to open
  const char* serverIP = argv[3];
  int serverPort = atoi(argv[4]);
  //printf("arg 3 %s\n",argv[3]);
  //printf("ip->%d port->%d\n",serverIP,serverPort );

  //open the pipes...........................
  int readfd, writefd;
  char pipe_name[20];
  snprintf(pipe_name,20,"/tmp/pipe%d0",worker_id);
  readfd = open(pipe_name, O_RDONLY);
  //open w_pipe for worker (pipe1)
  snprintf(pipe_name,20,"/tmp/pipe%d1",worker_id);
  if((writefd = open(pipe_name, O_WRONLY))<0)
  printf("error opening worker w_pipe \n" );
  //....................................................

  //read from pipe
  char buffer[20];
  read(readfd, buffer, 20);

  //take the first and the last num of the subdirs it will read
  const char s[2] = " ";
  char *token;
  char tmp_buf[10];
  strcpy(tmp_buf,buffer);
  //Get day
  token = strtok(tmp_buf, s);
  int first = atoi(tmp_buf);
  char *tmp = buffer;
  while(tmp[0]!=s[0])//skip the first
  {
    tmp++;
  }
  tmp++;//skip space
  int last = atoi(tmp);
  //printf("worker:%d first->%d last->%d\n",worker_id,first,last);


  //  opening parent directory
  DIR *parent_dir;
  parent_dir = opendir(input_dir);
  if(parent_dir==NULL)
  {
    printf("ERROR at opening parent_dir\n");
    return 0;
  }

  country_list_node* cntry_head = NULL;//create list head
  //read subdirectories
  struct dirent* sub_dir;
  int subdirectories_count = 1;
  while ((sub_dir = readdir(parent_dir)))
  {
    if (!strcmp (sub_dir->d_name, ".") || !strcmp (sub_dir->d_name, ".."))
            continue;
    if(subdirectories_count>= first && subdirectories_count <= last)
    {
      //add the subdirectories assigned there to a list
      cntry_head = insert_to_cntry_list(cntry_head,sub_dir->d_name,subdirectories_count);
    }
    subdirectories_count++;
  }






  //__________________________________________________________
  //__________________________________________________________
  //__________________________________________________________
  //__________________________________________________________
  country_list_node* ptr = cntry_head;
  countries = cntry_head;//global ptr for signals
  char sub_path[20];
  while (ptr!=NULL)//big while for every country
  {
    //open sub_dir
    //creating the sub_path
    strcpy(sub_path,input_dir);
    strcat(sub_path,"/");
    strcat(sub_path,ptr->country);
    //printf("worker %d---%s\n",worker_id,sub_path);

    //count how many .txt so i can make an array
    DIR *lo;
    lo = opendir(sub_path);
    if(lo==NULL)
    {
      printf("ERROR at opening parent_dir\n");
      return 0;
    }
    struct dirent* counter;
    int txt_counter = 0;
    while ((counter = readdir(lo)))
    {
      if (!strcmp (counter->d_name, ".") || !strcmp (counter->d_name, ".."))
              continue;
      txt_counter++;
    }
    //printf("this many->%d\n",txt_counter );
    closedir(lo);
    //....................................................


    //make array to sort the dates
    date** array;
    array = malloc(txt_counter*sizeof(date*));


    //opening sub directory
    DIR *subdirectory;
    subdirectory = opendir(sub_path);
    if(subdirectory==NULL)
    {
      printf("ERROR at opening parent_dir\n");
      return 0;
    }


    //read all .txt
    int ar = 0;
    while ((sub_dir = readdir(subdirectory)))
    {
      if (!strcmp (sub_dir->d_name, ".") || !strcmp (sub_dir->d_name, ".."))
              continue;
      //convert into dates str
      //put in array
      array[ar] = cnvrt_dates(sub_dir->d_name);
      //printf("%d-%d-%d\n",date_ptr->Day,date_ptr->Month,date_ptr->Year );
      //printf("%s\n",sub_dir->d_name );
      ar++;
    }

    //sort txt
    sort_array(array,txt_counter);
    // //print all array
    // for(int i=0;i<txt_counter;i++)
    // {
    //   printf("%d-%d-%d\n",array[i]->Day,array[i]->Month,array[i]->Year );
    // }

    //open all txt one by one
    list_stct* list;//create record list
    list =(list_stct*)malloc(sizeof(list_stct));
    list->head=NULL;
    list->tail= NULL;

    disease_list* d_list;
    d_list = (disease_list*)malloc(sizeof(disease_list));
    d_list->head=NULL;
    d_list->disease_count = 0;
    d_list->tail= NULL;


    char txt_path[30];
    for(int i = 0; i < txt_counter; i++)
    {
      //function to open txt and put recs in struct

      //make txt path
      strcpy(txt_path,sub_path);
      strcat(txt_path,"/");
      strcat(txt_path,make_txt_name(array[i]));
      // printf("%s\n",txt_path );

      open_txt(txt_path,list,d_list,array[i]);

      memset(txt_path,' ', 30);//clear the country array

    }
    //print lists
    // rec_node* test1 = list->head;
    //disease_node* test2 = d_list->head;
    // while(test1!=NULL)
    // {
    //   printf("%s\n",test1->last_name );
    //   test1 = test1->next;
    // }
    // while(test2!=NULL)
    // {
    //   printf("%s\n",test2->diseaseID);
    //   test2 = test2->next;
    // }


    //put them to struct
    memset(sub_path,' ', 20);//clear the country array
    //add diseaselist and reclist  to country_list_node
    ptr->disease_list = d_list;
    ptr->rec_list = list;
    ptr->dates_array = array;
    ptr->txt_counter = txt_counter;

    ptr = ptr->next;


  }

  //code to chech if the strucst are ok
  //____________________________________________________________
  //______________________________________________________________
  // date ** date_ptr;
  // ptr = cntry_head;
  // disease_node* disease_ptr = ptr->disease_list->head;
  // while (ptr!=NULL)//every country
  // {
    // disease_ptr = ptr->disease_list->head;
    // printf("%s\n",ptr->country);
    //   while (disease_ptr!=NULL)
    //   {
    //     printf("%s\n",disease_ptr->diseaseID );
    //     disease_ptr = disease_ptr->next;
    //   }

      // rec_node* rec_ptr = cntry_head->rec_list->head;
      //
      // while(rec_ptr!=NULL)
      // {
      //   printf("%d\n",rec_ptr->id );
      //   rec_ptr = rec_ptr->next;
      // }

    // date_ptr = ptr->dates_array;
    // //print all array
    // for(int i=0;i<ptr->txt_counter;i++)
    // {
    //   printf("%d-%d-%d\n",date_ptr[i]->Day,date_ptr[i]->Month,date_ptr[i]->Year );
    // }
  //   ptr = ptr->next;
  // }


  //____________________________________________________________
  //______________________________________________________________

  //connect to server
  int sock = make_connection_worker(serverPort,serverIP);
  int port, sock2, newsock;
  struct sockaddr_in server, client; //create server? and client?
  socklen_t clientlen;
  //pointer to structs
  struct sockaddr *serverptr=(struct sockaddr *)&server;
  struct sockaddr *clientptr=(struct sockaddr *)&client;
  struct hostent *rem;//?????????
  port = (6000+worker_id); //take the port num

  //create socket at the port num ??
  if ((sock2 = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    perror("socket");
    exit (-1);
  }
  //perror_exit("socket");
  server.sin_family = AF_INET; /* Internet domain */ //no idea what this is
  server.sin_addr.s_addr = htonl(INADDR_ANY);
  server.sin_port = htons(port); /* The given port */
  /* Bind socket to address */
  if (bind(sock2, serverptr, sizeof(server)) < 0)
  {
    perror("bind");
    exit (-1);
  }
  //perror_exit("bind");
  /* Listen for connections */
  //second param is maximum length to which the queue of
  //pending connections for sockfd may grow
  if (listen(sock2, 12) < 0)
  {
    perror("listen");
    exit (-1);
  }

  // port = getsockname(sock2,serverptr,&clientlen);
  // printf("port is -> %d\n", port);
  //__________________________________________________________
  //__________________________________________________________
  //__________________________________________________________
  //__________________________________________________________
  if(sock!=0)
  {
    //make stats
    int num_of_countries = (last - first +1);
    int cmp;
    disease_node* d_ptr;
    rec_node* rec_ptr;
    ptr = cntry_head;
    int zero,twenty,fourty,sixty;
    char tmp_buff[30];
    int wtf = 0;

    //strcpy(tmp_buff,"666");
    snprintf(tmp_buff,sizeof(tmp_buff),"%d",port);
    write(sock,tmp_buff,30);

    //send how many countries he will read
    snprintf(tmp_buff,sizeof(tmp_buff),"%d",num_of_countries);
    //printf("%s\n",tmp_buff );
    write(sock,tmp_buff,30);
    while(ptr!=NULL)//for every country
    {
      //send country
      //memset(tmp_buff,' ', 10);//clear the array
      strcpy(tmp_buff,ptr->country);
      //printf("country %s\n",tmp_buff );
      write(sock,tmp_buff,30);
      memset(tmp_buff,' ', 30);

      //send dates count
      snprintf(tmp_buff,sizeof(tmp_buff),"%d",ptr->txt_counter);
      //printf("dates count %s\n",tmp_buff );
      write(sock,tmp_buff,30);

      //send disease count
      snprintf(tmp_buff,sizeof(tmp_buff),"%d",ptr->disease_list->disease_count);
      write(sock,tmp_buff,30);
      //printf("diseases count %s\n",tmp_buff );

      for(int i = 0; i< ptr->txt_counter;i++)//for every date.txt
      {
        //send date
        snprintf(tmp_buff,sizeof(tmp_buff),"%d",ptr->dates_array[i]->Day);
        write(sock,tmp_buff,5);
        snprintf(tmp_buff,sizeof(tmp_buff),"%d",ptr->dates_array[i]->Month);
        write(sock,tmp_buff,5);
        snprintf(tmp_buff,sizeof(tmp_buff),"%d",ptr->dates_array[i]->Year);
        write(sock,tmp_buff,5);

        //printf("\n%d-%d-%d\n\n",ptr->dates_array[i]->Day,ptr->dates_array[i]->Month,ptr->dates_array[i]->Year );
        d_ptr = ptr->disease_list->head;
        while(d_ptr!=NULL)//for every disease
        {
          zero = 0;
          twenty = 0;
          fourty = 0;
          sixty = 0;

          //send disease
          write(sock,d_ptr->diseaseID,10);
          //printf("%s\n",d_ptr->diseaseID);
          rec_ptr = ptr->rec_list->head;//see top of rec list for every disease
          while(rec_ptr!=NULL)//for every record
          {

            cmp = datecmp(ptr->dates_array[i],rec_ptr->date);
              if(cmp == 3)
              {
                if(!strcmp(rec_ptr->diseaseID,d_ptr->diseaseID))
                {
                  if(rec_ptr->age >60)
                  {
                    sixty++;
                  }
                  else if(rec_ptr->age >40)
                  {
                    fourty++;
                  }
                  else if(rec_ptr->age > 20)
                  {
                    twenty++;
                  }
                  else
                  {
                    zero++;
                  }
                  //printf("%d-%d-%s-%s-%s-%d\n",rec_ptr->id,rec_ptr->enter_exit,rec_ptr->name,rec_ptr->last_name,rec_ptr->diseaseID,rec_ptr->age );
                }
              }
            rec_ptr = rec_ptr->next;
          }
          //send  stats
          snprintf(tmp_buff,sizeof(tmp_buff),"%d",zero);
          write(sock,tmp_buff,5);
          snprintf(tmp_buff,sizeof(tmp_buff),"%d",twenty);
          write(sock,tmp_buff,5);
          snprintf(tmp_buff,sizeof(tmp_buff),"%d",fourty);
          write(sock,tmp_buff,5);
          snprintf(tmp_buff,sizeof(tmp_buff),"%d",sixty);
          write(sock,tmp_buff,5);
          // printf("%d\n",zero );
          // printf("%d\n",twenty );
          // printf("%d\n",fourty );
          // printf("%d\n",sixty );
          d_ptr = d_ptr->next;
        }
      }
      ptr = ptr->next;
    }

    int worker_fd;
    while(1)
    {
      clientlen = sizeof(client); //?????????
      //__________________________
      /* accept connection */
      if ((worker_fd = accept(sock2, clientptr, &clientlen)) < 0)
      {
        perror("accept");
        exit (-1);
      }
      //printf("worker accepted connection\n" );
      char query[200];
      read(worker_fd,query,200);
      printf("%s\n",query );
      work_query(worker_fd,query,cntry_head);
      //write(worker_fd,"ok",3);
      //close(sock2);
    }



  }//if sock!= 0


 return 0;


}
