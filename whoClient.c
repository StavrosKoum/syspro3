#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include<sys/wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <signal.h>
#include "functions.h"
#include "functions2.h"
#include <sys/wait.h> /* sockets */
#include <sys/types.h> /* sockets */
#include <sys/socket.h> /* sockets */
#include <netinet/in.h> /* internet sockets */
#include <netdb.h> /* gethostbyaddr */
#include <pthread.h>  //for threads



// Declaration of thread condition variable
pthread_cond_t cond1 = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_main = PTHREAD_COND_INITIALIZER;

// declaring mutex
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t lock_main = PTHREAD_MUTEX_INITIALIZER;


void* thread_client_fun(void* arguments)
{
  struct client_arg* arg = arguments ;
  pthread_mutex_lock(&lock);//lock for wait.....
  char line[200];
  char test = '/';
  strcpy(line,arg->line);

  //signal main to start again
  pthread_mutex_lock(&lock_main);
  pthread_cond_signal(&cond_main);
  pthread_mutex_unlock(&lock_main);
  // let's wait on conition variable cond1
  //printf("going to bed now\n" );
  pthread_cond_wait(&cond1, &lock);
  // printf("i am awake\n");
  // release lock
  pthread_mutex_unlock(&lock);//unlock for wait....
  //printf("%s",line );
  if(line[0]!=test)
  {
    printf("error line\n" );
    return NULL;
  }
  //connect to server
  int sock = make_connection_worker(arg->servPort,arg->servIP);
  if(sock!=0)//so it wont write on stdo if connextion fails
  {
    // char key[1];
    // puts("Press any key to continue...");
    // key[0]= getchar();
    write(sock, line , 200);
    char i[4];
    char answer[100];
    read(sock,i,3);
    //printf("client took  option %s\n",i );
    int option = atoi(i);

    if(option==1)
    {
      read(sock,answer,100);
      printf("client took the message: %s",answer );
    }
    else if(option ==2 )
    {
      read(sock,answer,100);
      printf("client took the message: %s\n",answer );
    }
    else
    {
      printf("no answer\n" );
    }

  }

  return NULL;


}

int main(int argc,char* argv[])
{
  //Check the num of arguments
  if(argc!=9)
  {
    printf("Wrong arguments.Please try again\n");
    return 0;
  }

  const char* queryFile = argv[2];
  int numThreads = atoi(argv[4]);
  int servPort = atoi(argv[6]);
  const char* servIP = argv[8];

  //make n numThreads in array
  pthread_t * thread_arr = malloc(sizeof(pthread_t)*numThreads);

  //arg struct
  client_arg* arg = malloc(sizeof(client_arg));
  arg->servPort = servPort;
  arg->servIP = servIP;
  char buf[200];

  FILE* file;
  int num = 0;//to count threads
  file = fopen(queryFile,"r");
  if(file)
  {
    while(fgets(buf, 200, file))//while to read all
    {
      //read line

      //put to struct
      //sleep(1);
      arg->line = buf;
      //printf("%s\n",arg->line );

      //make thread
      pthread_create(&thread_arr[num], NULL,thread_client_fun,(void *) arg);
      //threads count++
      num++;
      //wait thread to sleep
      pthread_mutex_lock(&lock_main);
      pthread_cond_wait(&cond_main, &lock_main);
      pthread_mutex_unlock(&lock_main);

      //if count = num threads
      //wake them
      //pthread_join to wait them AND COUNT = 0
      if(num==numThreads)
      {
        //wake them
        pthread_mutex_lock(&lock);
        pthread_cond_broadcast(&cond1);
        pthread_mutex_unlock(&lock);
        //wait to finish
        for(int i = 0; i < numThreads;i++)
        {
          //printf("INSIDE\n" );
          pthread_join(thread_arr[i],NULL);
          //printf("INSIDE2\n" );
          num = 0;
        }
      }
      memset(buf,' ', 200);//clear the buf
    }
  }
  if(num!=0)//if there are there are threads left to wakr up
  {
    //wake them
    pthread_mutex_lock(&lock);
    pthread_cond_broadcast(&cond1);
    pthread_mutex_unlock(&lock);
    //wait to finish
    for(int i = 0; i < numThreads;i++)
    {
      pthread_join(thread_arr[i],NULL);
    }
  }


free(arg);

}
