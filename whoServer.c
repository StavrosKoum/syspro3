#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include<sys/wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <signal.h>
#include <sys/wait.h> /* sockets */
#include <sys/types.h> /* sockets */
#include <sys/socket.h> /* sockets */
#include <netinet/in.h> /* internet sockets */
#include <netdb.h> /* gethostbyaddr */
#include <pthread.h>  //for threads
#include <ctype.h> /* toupper */
#include "functions.h"
#include "functions2.h"

//./whoServer –q 5000 -s 4000 –w 4 –b 50
//./master –w 1 -b 45 –s 127.0.0.1 –p 4000 -i input_dir
//./whoClient -q options -w 2 -sp 5000 -sip 127.0.0.1

// Declaration of thread condition variable
pthread_cond_t cond1 = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_main = PTHREAD_COND_INITIALIZER;

// declaring mutex
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t lock_main = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t lock_buf = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t lock_print = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t lock_list = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t lock_worker = PTHREAD_MUTEX_INITIALIZER;



void* thread_server_fun(void* arguments)
{
  pthread_mutex_lock(&lock);//lock for wait.....
  struct server_arg* arg = arguments ;
  int task;
  char worker_ip[50];

  //strcpy(line,arg->line);

  //signal main to start again
  pthread_mutex_lock(&lock_main);
  pthread_cond_signal(&cond_main);
  pthread_mutex_unlock(&lock_main);

  while(1)
  {
    // let's wait on conition variable cond1
    //printf("going to bed now\n" );
    pthread_cond_wait(&cond1, &lock);
    // release lock
    pthread_mutex_unlock(&lock);//unlock for wait....
    //printf("i am awake\n");
    //do work and exit here if you have to
    pthread_mutex_lock(&lock_buf);
    int  fd = take_fd(arg->list,arg->bufferSize,&task);
    strcpy(worker_ip,arg->list->worker_ip);
    //printf("fd is ------>%d\n",fd );
    pthread_mutex_unlock(&lock_buf);

    //take fd
    //call function for read
    if(task==0)
    {
      //call worker function
      //printf(" thread found the worker i guess\n" );
      //mutex so the threads wont print together
      pthread_mutex_lock(&lock_print);
      int worker_port = take_stats(fd);
      pthread_mutex_unlock(&lock_print);
      //printf("%d\n",worker_port );
      pthread_mutex_lock(&lock_list);
      insert_to_worker_list(worker_port,arg->worker_list);
      pthread_mutex_unlock(&lock_list);
      // worker_list_node * ptr = arg->worker_list->head; //print list
      // while(ptr!=NULL)
      // {
      //   printf("%d\n",ptr->port );
      //   ptr = ptr->next;
      // }
    }
    if(task==1)
    {
      //call client function
      pthread_mutex_lock(&lock_print);
      read_client(fd,arg->worker_list,worker_ip);
      pthread_mutex_unlock(&lock_print);
      //printf("lol\n");
    }
    if(task!=1 && task!=0)
    {
      printf("big problem at server_thread!!!!!\n" );
    }
    printf("Closing connection.\n");
    pthread_mutex_lock(&lock_print);
    close(fd);
    pthread_mutex_unlock(&lock_print);


    // //signal main to start again
    // pthread_mutex_lock(&lock_main);
    // pthread_cond_signal(&cond_main);
    // pthread_mutex_unlock(&lock_main);


    //this will unlock after the while in wait
    pthread_mutex_lock(&lock);//lock for wait.....AGAIN beacuse of the loop
  }



  return NULL;


}



int main(int argc,char* argv[])
{
  //Check the num of arguments
  if(argc!=9)
  {
    printf("Wrong arguments.Please try again\n");
    return 0;
  }


  int queryPortNum = atoi(argv[2]);
  int statisticsPortNum = atoi(argv[4]);
  int numThreads = atoi(argv[6]);
  int bufferSize = atoi(argv[8]);

  // printf("queryPortNum %d\n",queryPortNum );
  // printf("statisticsPortNum %d\n", statisticsPortNum);
  // printf("numThreads %d\n", numThreads);
  // printf("bufferSize %d\n", bufferSize);

  //need a mutex for the buffer
  circular_buf_list* fd_buf = make_circular_buffer(bufferSize);
  circular_buf_node* ptr = fd_buf->head;



  //__________________________________________________________
  //__________________________________________________________
  //__________________________________________________________
  //MAKE THE SERVER!!!!!!!!!!!!!!!!!!!!!!!
  int stat_port,client_port, stat_sock,client_sock, newsock;
  struct sockaddr_in server, client,server2; //create server? and client?
  socklen_t clientlen;

  //pointer to structs
  struct sockaddr *serverptr=(struct sockaddr *)&server;
  struct sockaddr *clientptr=(struct sockaddr *)&client;
  struct sockaddr *serverptr2=(struct sockaddr *)&server2;//useless


  struct hostent *rem;//?????????
  stat_port = statisticsPortNum; //take the port num
  client_port = queryPortNum;


  //create socket at the port num ??
  if ((stat_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    perror("socket");
    exit (-1);
  }
  server.sin_family = AF_INET; /* Internet domain */ //no idea what this is
  server.sin_addr.s_addr = htonl(INADDR_ANY);
  server.sin_port = htons(stat_port); /* The given port */

  /* Bind socket to address */
  if (bind(stat_sock, serverptr, sizeof(server)) < 0)
  {
    perror("bind");
    exit (-1);
  }
  /* Listen for connections */
  //second param is maximum length to which the queue of
  //pending connections for sockfd may grow
  if (listen(stat_sock, 12) < 0)
  {
    perror("listen");
    exit (-1);
  }

  //CREATE SOCK FOR client
  //__________________________________________________________
  //__________________________________________________________
  //create socket at the port num ??
  if ((client_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    perror("socket");
    exit (-1);
  }
  server.sin_family = AF_INET; /* Internet domain */ //no idea what this is
  server.sin_addr.s_addr = htonl(INADDR_ANY);
  server.sin_port = htons(client_port); /* The given port */

  /* Bind socket to address */
  if (bind(client_sock, serverptr, sizeof(server2)) < 0)
  {
    perror("bind");
    exit (-1);
  }
  //printf("PROBLEMMMM22222\n" );
  //perror_exit("bind");

  /* Listen for connections */
  //second param is maximum length to which the queue of
  //pending connections for sockfd may grow
  if (listen(client_sock, 12) < 0)
  {
    perror("listen");
    exit (-1);
  }

  // create sock array
  int sock_array[2];
  sock_array[0]= stat_sock;
  sock_array[1]= client_sock;
  int arr_pos;
  int fd;

  //create list for workers port
  worker_list* list =  make_worker_list();

  //make arg struct
  struct server_arg* arg = malloc(sizeof(server_arg));
  arg->list = fd_buf;
  arg->bufferSize = bufferSize;
  arg->worker_list = list;

  //make n numThreads in array
  pthread_t * thread_arr = malloc(sizeof(pthread_t)*numThreads);

  for(int i =0; i <numThreads;i++)
  {
    //make thread
    pthread_create(&thread_arr[i], NULL,thread_server_fun,(void *)arg);
    //wait thread to sleep
    pthread_mutex_lock(&lock_main);
    pthread_cond_wait(&cond_main, &lock_main);
    pthread_mutex_unlock(&lock_main);

  }


  int flag = 0 ;
  while (1)
  {
    clientlen = sizeof(client); //?????????

    if((fd = network_accept_any(sock_array, 2, clientptr, &clientlen, &arr_pos)) < 0)
    {
            printf("errrrroooor\n" );
            exit(-1);
    }
    // /* accept connection */
    // if ((newsock = accept(stat_sock, clientptr, &clientlen)) < 0)
    // printf("PROBLEMMMM4\n" );
    // //perror_exit("accept");

    /* Find client's name */
    if ((rem = gethostbyaddr((char *) &client.sin_addr.s_addr,sizeof(client.sin_addr.s_addr), client.sin_family))== NULL)
    {
      herror("gethostbyaddr"); exit(1);
    }
    printf("Accepted connection from %s\n", rem->h_name);
    if(arr_pos==0)
    {
      printf("Worker connected\n" );
      if(flag == 0)
      {//put worker ip to struct
        pthread_mutex_lock(&lock_buf);
        fd_buf->worker_ip = rem->h_name;
        //printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1\n" );
        pthread_mutex_unlock(&lock_buf);
        flag = 1;
      }

    }
    if(arr_pos==1)
    {
      printf("Client connected\n" );
    }


    //add fd to buffer
    pthread_mutex_lock(&lock_buf);
    insert_fd_to_buffer(fd_buf,fd,bufferSize,arr_pos);
    pthread_mutex_unlock(&lock_buf);


    pthread_mutex_lock(&lock);
    //wake up one thread
    pthread_cond_signal(&cond1);
    pthread_mutex_unlock(&lock);

    // pthread_mutex_lock(&lock_main);
    // pthread_cond_wait(&cond_main, &lock_main);
    // pthread_mutex_unlock(&lock_main);


  }



//free arg_struct!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
return 0;










}
