#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include<sys/wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <signal.h>
#include <sys/wait.h> /* sockets */
#include <sys/types.h> /* sockets */
#include <sys/socket.h> /* sockets */
#include <netinet/in.h> /* internet sockets */
#include <netdb.h> /* gethostbyaddr */
#include <netinet/in.h> /* internet sockets */
#include <pthread.h>  //for threads
#include "functions.h"//maybe useless
#include "functions2.h"


int make_connection_worker(int serverPort,const char* serverIP)
{

  int port, sock, i;
  char buf[256];
  struct sockaddr_in server;
  struct sockaddr *serverptr = (struct sockaddr*)&server;
  struct hostent *rem;

  /* Create socket */
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  printf("PROBLEMMMM\n" );
  //perror_exit("socket");

  //printf("ip->>>%s\n",serverIP );
  /* Find server address */
  if ((rem = gethostbyname(serverIP)) == NULL)
  {
    herror("gethostbyname"); exit(1);
  }
  port = serverPort;
  server.sin_family = AF_INET; /* Internet domain *///!!!!!!!!!!!!!!!!!!!!!!!!!
  memcpy(&server.sin_addr, rem->h_addr, rem->h_length);
  server.sin_port = htons(port); /* Server port */
  /* Initiate connection */
  if (connect(sock, serverptr, sizeof(server)) < 0)
  {
    printf("did not connect\n" );
    return 0;
  }

  //perror_exit("connect");
//  printf("Connecting to  port %d\n", port);
  //fgets(buf, sizeof(buf), stdin); /* Read from stdin*/

  return sock;
}

int make_connection_server(int statisticsPortNum ) //NOT READY NOT USED
{
  int port, sock, newsock;
  struct sockaddr_in server, client; //create server? and client?
  socklen_t clientlen;

  //pointer to structs
  struct sockaddr *serverptr=(struct sockaddr *)&server;
  struct sockaddr *clientptr=(struct sockaddr *)&client;

  struct hostent *rem;//?????????
  port = statisticsPortNum; //take the port num

  //create socket at the port num ??
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  printf("PROBLEMMMM1\n" );
  //perror_exit("socket");
  server.sin_family = AF_INET; /* Internet domain */ //no idea what this is
  server.sin_addr.s_addr = htonl(INADDR_ANY);
  server.sin_port = htons(port); /* The given port */

  /* Bind socket to address */
  if (bind(sock, serverptr, sizeof(server)) < 0)
  printf("PROBLEMMMM2\n" );
  //perror_exit("bind");

  /* Listen for connections */
  //second param is maximum length to which the queue of
  //pending connections for sockfd may grow
  if (listen(sock, 5) < 0)
  printf("PROBLEMMMM3\n" );
  //perror_exit("listen");

  while (1)
  {
    clientlen = sizeof(client); //?????????

    /* accept connection */
    if ((newsock = accept(sock, clientptr, &clientlen)) < 0)
    printf("PROBLEMMMM4\n" );
    //perror_exit("accept");

    /* Find client's name */
    if ((rem = gethostbyaddr((char *) &client.sin_addr.s_addr,sizeof(client.sin_addr.s_addr), client.sin_family))== NULL)
    {
      herror("gethostbyaddr"); exit(1);
    }
    printf("Accepted connection from %s\n", rem->h_name);

    // serving client WILL BE INSIDE AThread!!!!!!!!
    char buf[1];
    while(read(newsock, buf, 1) > 0)
    { /* Receive 1 char */
      putchar(buf[0]); /* Print received char */
      printf("\n");
      // /* Capitalize character */
      // buf[0] = toupper(buf[0]);
      // /* Reply */
      // if (write(newsock, buf, 1) < 0)
      // printf("PROBLEMMMM5\n" );
      //perror_exit("write");
    }
    printf("Closing connection.\n");
    close(newsock); /* Close socket */

  }

}

circular_buf_list* make_circular_buffer(int num)
{
  circular_buf_list* list = malloc(sizeof(circular_buf_list));

  list->head = malloc(sizeof(circular_buf_node));
  list->head->used = 0;
  list->head->next= NULL;
  list->head->fd= 666;
  num--;
  circular_buf_node* ptr = list->head;
  for(int i =0; i < num; i++)
  {
    ptr->next = malloc(sizeof(circular_buf_node));
    ptr = ptr->next;
    list->tail = ptr;
    ptr->used = 0;
    ptr->next = NULL;
    ptr->fd= 666;
  }
  list->tail->next = list->head;
  return list;
}

int  insert_fd_to_buffer(circular_buf_list* list,int fd,int bufferSize,int task)
{
  circular_buf_node * temp = list->head;
  if(list->full==1)
  {
    printf("buf is full. I cant insert the fd\nClosing connection" );

    close(fd);
    return 0;
  }

  int count = 0;
  while(count < bufferSize)
  {
    //check the nodes. if count== buffersize we made a circle
    if(temp->used == 0)
    {
      //insert
      //printf("---->%d\n",count );
      temp->fd = fd;
      temp->used = 1;
      temp->task = task;
      if(count == (bufferSize-1))//means we used the last list node in the buff
      {
        list->full = 1;
      }
      return 0;
    }
    temp = temp-> next;
    count++;

  }

}

int network_accept_any(int  fds[], unsigned int count,struct sockaddr *addr, socklen_t *addrlen,int * pos)
{
    fd_set readfds;
    int maxfd, fd;
    unsigned int i;
    int status;

    FD_ZERO(&readfds);
    maxfd = -1;
    for (i = 0; i < count; i++)
    {
        FD_SET(fds[i], &readfds);
        if (fds[i] > maxfd)
            maxfd = fds[i];
    }
    status = select(maxfd + 1, &readfds, NULL, NULL, NULL);
    if (status < 0)
    {
      return (-1);
    }
    fd = (-1);
    for (i = 0; i < count; i++)
        if (FD_ISSET(fds[i], &readfds))
        {
            fd = fds[i];
            break;
        }
    if (fd == (-1))
    {
      return (-1);
    }
    else
    {
      *pos = i;
      return accept(fd, addr, addrlen);
    }

}

int take_fd(circular_buf_list* list,int bufferSize,int* task)
{
  circular_buf_node* ptr = list->head;
  if(list->empty==1)
  {
    printf("buf is full\n" );
    return 0;
  }
  int count = 0;
  while(count < bufferSize)
  {
    if(ptr->used==1)//we found the firts node with an fd
    {
      if(count==0)
      {
        list->empty ==1;
      }
      ptr->used=0;//make it empty
      *task = ptr->task;
      return ptr->fd;
    }
    count++;
    ptr = ptr->next;
  }
  printf("problem at take_fd\n" );
  return 0;

}

int take_stats(int readfd)
{
  int countries_to_read;
  int dates_to_read,diseases_to_read;
  int country_num;
  char temp[30];
  char country[30];
  int day,month,year;
  int zero,twenty,fourty,sixty;

  read(readfd,temp,30);
  int worker_port = atoi(temp);
  //mutex so the threads wont print together
  read(readfd,temp,30);
  countries_to_read = atoi(temp);
  for(int k = 0; k < countries_to_read; k++)
  {
    read(readfd,country,30);//read the country
    //printf("_____%s_____\n",country );
    read(readfd,temp,30);//read how many dates i have to read
    dates_to_read = atoi(temp);
    read(readfd,temp,30);//read how many diseases  i have to read
    diseases_to_read = atoi(temp);
    // printf("dates to read %d\n", dates_to_read);
    // printf("disease to read %d\n",diseases_to_read );
    for(int j = 0; j < dates_to_read; j++)//loop to read all dates
    {
      read(readfd,temp,5);
      day = atoi(temp);
      read(readfd,temp,5);
      month = atoi(temp);
      read(readfd,temp,5);
      year = atoi(temp);
      //printf("\n%d-%d-%d\n",day,month,year);//------------------------------
      //printf("%s\n",country );//----------------------------------------
      for(int h = 0; h < diseases_to_read; h++)//loop to read all disease
      {
        read(readfd,temp,10);//disease
        //printf("%s\n",temp );//----------------------------------------

        read(readfd,temp,5);
        zero = atoi(temp);
        read(readfd,temp,5);
        twenty = atoi(temp);
        read(readfd,temp,5);
        fourty = atoi(temp);
        read(readfd,temp,5);
        sixty = atoi(temp);
        // printf("Age range 0-20 years: %d cases\n",zero );
        // printf("Age range 20-40 years: %d cases\n",twenty );
        // printf("Age range 40-60 years: %d cases\n",fourty );
        // printf("Age range 60+ years: %d cases\n",sixty );
      }
    }

  }


  printf("i took the stats\n" );
  //read port num

  //return worker port
  return worker_port;
}

int read_client(int client_fd,worker_list* list,char* worker_ip)
{
  char query[200];
  char answer[4];
  read(client_fd,query,200);
  //printf("Query is:\n%s",query );
  worker_list_node * ptr = list->head; //print list
  //printf("worker ip is %s\n",worker_ip );
  int worker_fd,option,took_reply,num,plus,count;
  took_reply  =0;
  count = 0;
  while(ptr!=NULL)
  {

    //printf("%d\n",ptr->port );
    worker_fd = make_connection_worker(ptr->port,worker_ip);
    //printf("%d-------------------\n",ptr->port);
    if(worker_fd==0)
    {
      printf("worker did not connect\n" );
      return 0;
    }
    write(worker_fd,query,200);
    read(worker_fd,answer,3);
    option = atoi(answer);
    num = ser_handle_options(option,client_fd,worker_fd,&plus);
    took_reply = took_reply + num;
    count = count + plus;

    //printf("option %d\n",option );
    //printf("%d________________\n",ptr->port);
    ptr = ptr->next;
  }
  //printf("took reply is ->%d \n",took_reply );
  if(took_reply==0 && option != 2 )
  {
    //means client took no reply
    write(client_fd,"0",3);//write to client
    return 0;
  }
  if(option == 2 )
  {
      write(client_fd,"2",3);//write to client
      snprintf(query,sizeof(query),"diseaseFrequency count is %d",count);
      write(client_fd,query,200);
  }

  return 0;
}

worker_list* make_worker_list()
{
  worker_list* list = malloc(sizeof(worker_list));
  list->head = NULL;
  list->tail = NULL;
  return list;
}

int insert_to_worker_list(int port,worker_list* list)
{
  worker_list_node* ptr = list->head;

  if(ptr==NULL)
  {
    ptr = malloc(sizeof(worker_list_node));
    ptr->port = port;
    ptr->next = NULL;
    list->head = ptr;
    list->tail = ptr;
  }
  else
  {
    while(ptr->next!= NULL)
    {
      ptr = ptr->next;
    }
    ptr->next = malloc(sizeof(worker_list_node));
    ptr = ptr->next;
    ptr->port = port;
    ptr->next = NULL;
    list->tail = ptr;
  }
  return 0;
}

int work_query(int worker_fd,char * buffer,country_list_node* country_list)
{

  char tmp_buf[100];
  const char s[2] = " ";//for strtok
  const char t[2] = "\n";
  const char a[2] = "-";
  char *token;
  char* ptr = tmp_buf;
  strcpy(tmp_buf,buffer);
  token = strtok(tmp_buf, s);//take the first word
  if(!strcmp(tmp_buf,"/searchPatientRecord"))
  {
    char lol[2];
    //take the record_id
    ptr = buffer;
    while (ptr[0] != s[0])//skip option
    {
      ptr++;
    }
    while (ptr[0]==s[0])//skip the spaces
    {
      ptr++;
    }
    strcpy(tmp_buf,ptr);//leave out the option
    strcpy(buffer,tmp_buf);//leave out the option
    ptr = tmp_buf;
    token = strtok(tmp_buf,s);//now temp has only the rec_id
    //printf("----------%s\n",tmp_buf );

    int rec_id = atoi(tmp_buf);
    //find it here

    country_list_node* ptr = country_list;
    rec_node* rec_ptr;
    rec_node* search_exit;
    int exit,flag = 0;
    while(ptr!=NULL)//for every country the worker has
    {
      rec_ptr = ptr->rec_list->head;
      while(rec_ptr!=NULL)//search every record
      {
        if(rec_id==rec_ptr->id && rec_ptr->enter_exit==0)//if its an enter record
        {
          //print
          // printf("%d %s %s %s %d",rec_ptr->id,rec_ptr->name,rec_ptr->last_name,rec_ptr->diseaseID,rec_ptr->age );
          // printf("%d-%d-%d ",rec_ptr->date->Day,rec_ptr->date->Month,rec_ptr->date->Year);
          // printf("--\n" );

          //write here
          write(worker_fd,"1",3);//zero means nothing to read
          snprintf(tmp_buf,sizeof(tmp_buf),"%d %s %s %s %d %d-%d-%d --\n",rec_ptr->id,rec_ptr->name,rec_ptr->last_name,rec_ptr->diseaseID,rec_ptr->age,rec_ptr->date->Day,rec_ptr->date->Month,rec_ptr->date->Year);
          write(worker_fd,tmp_buf,100);//zero means nothing to read
          return 0;
        }
        rec_ptr = rec_ptr->next;
      }
      ptr = ptr->next;
    }

    write(worker_fd,"0",3);//zero means nothing to read
    return 0;

  }
  else if(!strcmp(tmp_buf,"/diseaseFrequency"))
  {
    date* date1,*date2;
    date1 = (date*)malloc(sizeof(date));
    date2 = (date*)malloc(sizeof(date));
    char cntry[20];
    char disease[20];
    int country_val =0;
    //take the disease
    ptr = buffer;
    while (ptr[0] != s[0])//skip ortion
    {
      ptr++;
    }
    while (ptr[0]==s[0])//skip the spaces
    {
      ptr++;
    }
    strcpy(tmp_buf,ptr);//leave out the option
    strcpy(buffer,tmp_buf);//leave out the option
    ptr = tmp_buf;
    token = strtok(tmp_buf,s);//now temp has only the disease
    strcpy(disease,token);

    //take the country or date
    ptr = buffer;
    while (ptr[0] != s[0])//skip disease
    {
      ptr++;
    }
    while (ptr[0]==s[0])//skip the spaces
    {
      ptr++;
    }
    strcpy(tmp_buf,ptr);//leave out the disease
    strcpy(buffer,tmp_buf);//leave out the disease
    ptr = tmp_buf;

    //take the date1 now
    token = strtok(ptr,a);//now temp has only the entryDate.date
    date1->Day = atoi(token);

    //Get month
    char*tmp;
    strncpy(tmp_buf, buffer, 20);//take the whole date again
    tmp = tmp_buf;
    while(tmp[0]!=a[0])//skip the Day
    {
      tmp++;
    }
    tmp++;
    token = strtok(tmp, a);
    date1->Month = atoi(token);
    //Get year
    strncpy(tmp_buf, buffer, 20);//take the whole date again
    tmp = tmp_buf;
    while(tmp[0]!=a[0])//skip the Day
    {
      tmp++;
    }
    tmp++;
    while(tmp[0]!=a[0])//skip the Month
    {
      tmp++;
    }
    tmp++;
    token = strtok(tmp, a);
    date1->Year = atoi(token);

    //take the  date2_____________________________________________
    ptr = buffer;
    while (ptr[0] != s[0])//skip date1
    {
      ptr++;
    }
    while (ptr[0]==s[0])//skip the spaces
    {
      ptr++;
    }
    strcpy(tmp_buf,ptr);//leave out the Date1
    strcpy(buffer,tmp_buf);//both have the data after the Date1
    ptr = tmp_buf;
    token = strtok(ptr,s);//now temp has only the Date2

    //Get day
    token = strtok(tmp_buf, a);
    date2->Day = atoi(token);
    //Get month
    strncpy(tmp_buf, buffer, 20);//take the whole date again
    tmp = tmp_buf;
    while(tmp[0]!=a[0])//skip the Day
    {
      tmp++;
    }
    tmp++;
    token = strtok(tmp, a);
    date2->Month = atoi(token);
    //Get year
    strncpy(tmp_buf, buffer, 20);//take the whole date again
    tmp = tmp_buf;
    while(tmp[0]!=a[0])//skip the Day
    {
      tmp++;
    }
    tmp++;
    while(tmp[0]!=a[0])//skip the Month
    {
      tmp++;
    }
    tmp++;
    token = strtok(tmp, a);
    date2->Year = atoi(token);

    ptr = tmp_buf;
    while (ptr[0] != s[0])//skip date2
    {
      ptr++;
    }
    while (ptr[0]==s[0])//skip the spaces
    {
      ptr++;
    }

    if(ptr[0]>65 && ptr[0]<122)//if there IS a character after the spaces so there is a country
    {
      //take the country
      token = strtok(ptr,s);//now temp has only the country
      strcpy(cntry,token);
      country_val=1;
    }
    // printf("%s %s\n",disease,cntry );
    // printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
    // printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );
    //call function
    if(country_val==1)//with country
    {
      //diseaseFrequency1(list ,disease,date1,date2,numWorkers,fd_array);
      write(worker_fd,"0",3);//zero means nothing to read

    }
    else
    {

      //diseaseFrequency(list ,disease,cntry,date1,date2,numWorkers,fd_array);
      //for every country
      country_list_node* ptr = country_list;
      int counter = 0;
      int h;
      rec_node* rec_ptr;
      while(ptr!=NULL)//country loop
      {
        rec_ptr = ptr->rec_list->head;

        while(rec_ptr!=NULL)
        {
          if(!strcmp(disease,rec_ptr->diseaseID))//check disease
          {

            h = datecmp(rec_ptr->date,date2);//check dates
            if(h>0)
            {
              h = datecmp(rec_ptr->date,date1);
              if(h==0 || h==3)
              {
                //printf("%s %d-%d-%s-%s-%s-%d\n",ptr->country,rec_ptr->id,rec_ptr->enter_exit,rec_ptr->name,rec_ptr->last_name,rec_ptr->diseaseID,rec_ptr->age );
                counter++;
              }
            }

          }
          rec_ptr = rec_ptr->next;
        }
        ptr = ptr->next;
      }

      //sent count to server
      write(worker_fd,"2",3);//2 is for this case
      snprintf(tmp_buf,sizeof(tmp_buf),"%d",counter);
      write(worker_fd,tmp_buf,4);

      // free(date1);
      // free(date2);

    }

    free(date1);
    free(date2);

    return 0;
  }
  else//error
  {
    //if else no option read
    write(worker_fd,"0",3);//zero means nothing to read
    return 0;
  }

}

int ser_handle_options(int option,int client_fd,int worker_fd,int* count)
{
  char answer[100];
  if(option==0)
  {
    //do nothing client does not have to read anything
    return 0;

  }
  else if(option ==1)//serach patient record
  {
    //printf("tooooooooooooook 1\n" );
    write(client_fd,"1",3);//write to client
    read(worker_fd,answer,100);
    write(client_fd,answer,100);
    return 1;
  }
  else if(option ==2)///diseaseFrequency without country
  {

      read(worker_fd,answer,100);
      *count = atoi(answer);
      return 0;
  }
  else
  {
    //do nothing client does not have to read anything
    return 0;
  }
}
