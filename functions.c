#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include<sys/wait.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <signal.h>
#include "functions.h"

void print_list(country_list_node* head)
{
  country_list_node *ptr;
  ptr= head;
  while(ptr!=NULL)
  {
    printf("sub_dir is ->%s\n",ptr->country);
    ptr = ptr->next;
  }
}

country_list_node* insert_to_cntry_list(country_list_node* head,char* country,int country_num)
{
  country_num--;
  country_list_node* ptr = head;
  if(head==NULL)
  {
    head = malloc(sizeof(country_list_node));
    head->next = NULL;
    head->country_id = country_num;
    strcpy(head->country,country);
  }
  else
  {
    while (ptr->next != NULL)
    {
      ptr = ptr->next;
    }
    ptr->next = malloc(sizeof(country_list_node));
    ptr = ptr->next;
    ptr->country_id = country_num;
    ptr->next = NULL;
    strcpy(ptr->country,country);
  }
  return head;
}

void insert_to_list(list_stct* list,rec_node *rec)
{

  if(list->head==NULL)
  {
    list->head = rec;
    list->tail = rec;
  }
  else
  {

    list->tail->next = rec;
    list->tail = rec;
    //printf("%s\n",list->head->last_name );
  }
}
date* cnvrt_dates(char* buf)
{
  const char s[2] = "-";
  char *token;
  const char k[2] = ".";
  char tmp_buf[20];//tmp buffer so i can keep the whole date in the other one
  strncpy(tmp_buf, buf, 20);
  date * mydate = malloc(sizeof(date));
  char*tmp;


  //Get day
  token = strtok(tmp_buf, s);
  mydate->Day = atoi(token);
  //Get month
  strncpy(tmp_buf, buf, 20);//take the whole date again
  tmp = tmp_buf;
  while(tmp[0]!=s[0])//skip the Day
  {
    tmp++;
  }
  tmp++;
  token = strtok(tmp, s);
  mydate->Month = atoi(token);
  //Get year
  strncpy(tmp_buf, buf, 20);//take the whole date again
  tmp = tmp_buf;
  while(tmp[0]!=s[0])//skip the Day
  {
    tmp++;
  }
  tmp++;
  while(tmp[0]!=s[0])//skip the Month
  {
    tmp++;
  }
  tmp++;
  token = strtok(tmp, k);
  mydate->Year = atoi(token);

  return mydate;
}


int datecmp(date* date1,date* date2)
{
  if(date1->Year > date2->Year)
  return 0;//date1>date2
  if(date1->Year < date2->Year)
  return 1;//date2 > date 1
  if(date1->Year == date2->Year)
  {
    if(date1->Month > date2->Month)
    return 0;//date1>date2
    if(date1->Month < date2->Month)
    return 1;//date2 > date 1
    if(date1->Month == date2->Month)
    {
      if(date1->Day > date2->Day)
      return 0;//date1>date2
      if(date1->Day < date2->Day)
      return 1;//date2 > date 1
      if(date1->Day == date2->Day)
      {
        //printf("THOSE DATES ARE EQUAL\n");
        return 3; //means same dates
      }
    }
  }

}

void sort_array(date** arr, int n)
{
  int i, j, min;
  int tmp;
  for (i = 0; i < n - 1; i++)
  {
      // find the minimum
      min = i;
      for (j = i + 1; j < n; j++)
      {
        tmp = datecmp(arr[min],arr[j]);
        if(tmp == 0)
        min = j;
      }
      // Swap the  minimum with the first
      swap(arr[min], arr[i]);
  }
}

void swap(date* xp, date* yp)
{
    date temp = *xp;
    *xp = *yp;
    *yp = temp;
}

int find_enter(int id,list_stct* list)
{
  rec_node* ptr = list->head;
  // if(ptr == NULL)
  // {
  //   return 0;
  // }
  while (ptr!=NULL)
  {
    if(ptr->id==id)
    {
      //printf("lllllllllllll\n" );
      return 1;
    }
    ptr = ptr->next;
  }
  return 0;
}
void open_txt(char* txt_name,list_stct * list,disease_list* d_list,date* date)
{

  //printf("%s\n",txt_name );
  FILE* file;
  file = fopen(txt_name,"r");
  char buf[20];//buf to temporary keep the data
  rec_node * rec;//make a rec node

  if(file)
  {
    while(fscanf(file, "%s", buf) != EOF)//while to read all the entries and insert to list
    {
        rec=(rec_node*)malloc(sizeof(rec_node));
        //get the id
        int i = atoi(buf);
        rec->id = i;
        //get enter or exit
        fscanf(file, "%s", buf);
        if(!strcmp("EXIT",buf))
        {
          int k =find_enter(i,list);
          if(k)
          {
            rec->enter_exit = 1;//exit
          }
          else
          {
            rec->enter_exit = 3; //its an error record
            //printf("ERROR\n" );
          }

        }
        else
        {
          rec->enter_exit = 0;//enter
        }
        //get the first name
        fscanf(file, "%s", buf);
        strcpy(rec->name,buf);
        //get the last name
        fscanf(file, "%s", buf);
        strcpy(rec->last_name,buf);
        //get the diseaseID
        fscanf(file, "%s", buf);
        strcpy(rec->diseaseID,buf);
        //get age
        fscanf(file, "%s", buf);
        int age = atoi(buf);
        rec->age = age;
        rec->next = NULL;
        rec->date = date;

        // if(rec->enter_exit==3)
        // printf("%d-%d-%s-%s-%s-%d\n",rec->id,rec->enter_exit,rec->name,rec->last_name,rec->diseaseID,rec->age );
        // if(list->head!=NULL)
        // printf("%s\n",list->head->last_name );
        if(rec->enter_exit!=3)
        {
          insert_to_list(list,rec);
          //printf("%s\n",list->head->last_name );
          insert_to_disease_list(d_list,rec->diseaseID);
        }

    }

  }
  else
  {
    printf("File did not open\n");
  }


}


int insert_to_disease_list(disease_list* list,char* diseaseID)
{
  disease_node * ptr = list->head;
  if(list->head==NULL)
  {

    list->head = malloc(sizeof(disease_node));
    strcpy(list->head->diseaseID,diseaseID);
    list->tail = list->head;
    list->disease_count++;
  }
  else
  {
    while(ptr!=NULL)
    {
      //check if there is the same disease
      if(!strcmp(diseaseID,ptr->diseaseID))
      {
        return 0;
      }
      ptr = ptr->next;
    }
    list->disease_count++;
    list->tail->next = malloc(sizeof(disease_node));
    strcpy(list->tail->next->diseaseID,diseaseID);
    list->tail = list->tail->next;


    // while (ptr!=list->tail)
    // {
    //   //check if there is the same disease
    //   if(!strcmp(diseaseID,ptr->diseaseID))
    //   {
    //     return 0;
    //   }
    //   ptr = ptr->next;
    // }
    // if(ptr == list->tail && (!strcmp(diseaseID,ptr->diseaseID)))//if i found same diseaseID
    // {
    //   //do nothing
    //   return 0;
    // }
    // else
    // {
    //   ptr->next = malloc(sizeof(disease_node));
    //   list->tail = ptr->next;
    // }
    return 1;


  }
}

void insert_dirs_to_list(country_parent_list*list,int first,int last,int worker_id)
{
  country_parent_node* ptr = list->head;

  first --;
  last --;
  while(ptr!=NULL)
  {
    if(ptr->country_num >= first && ptr->country_num<=last)
    {
      //printf("%d >= %d <= %d\n",first,ptr->country_num,last);
      ptr->worker_id = worker_id;
    }
    ptr = ptr->next;
  }
}

void print_country(country_parent_list* list,int id)
{
  country_parent_node* ptr = list->head;
  while (ptr!=NULL)
  {
    if(ptr->country_num == id)
    {
      printf("%s\n",ptr->country);
      break;
    }
    ptr = ptr->next;
  }
}

void get_stats(int readfd,int worker,country_parent_list* country_list)
{
  int countries_to_read;
  int dates_to_read,diseases_to_read;
  int country_num;
  char temp[20];
  int day,month,year;
  int zero,twenty,fourty,sixty;

  read(readfd,temp,10);
  countries_to_read = atoi(temp);
  //printf("have to read %d countries \n",countries_to_read );
  for(int k = 0; k < countries_to_read; k++)
  {
    read(readfd,temp,10);//read the country
    country_num = atoi(temp);

    read(readfd,temp,10);//read how many dates i have to read
    dates_to_read = atoi(temp);
    //printf("dates to read %d\n", dates_to_read);
    read(readfd,temp,5);//read how many diseases  i have to read
    diseases_to_read = atoi(temp);
    //printf("disease to read %d\n",diseases_to_read );
    for(int j = 0; j < dates_to_read; j++)//loop to read all dates
    {
      //printf("\n" );
      read(readfd,temp,5);
      day = atoi(temp);
      read(readfd,temp,5);
      month = atoi(temp);
      read(readfd,temp,5);
      year = atoi(temp);
      printf("\n%d-%d-%d\n",day,month,year);//------------------------------
      print_country(country_list,country_num);//---------------------------------

      for(int h = 0; h < diseases_to_read; h++)//loop to read all disease
      {
        read(readfd,temp,10);
        printf("%s\n",temp );//----------------------------------------

        read(readfd,temp,5);
        zero = atoi(temp);
        read(readfd,temp,5);
        twenty = atoi(temp);
        read(readfd,temp,5);
        fourty = atoi(temp);
        read(readfd,temp,5);
        sixty = atoi(temp);
        printf("Age range 0-20 years: %d cases\n",zero );
        printf("Age range 20-40 years: %d cases\n",twenty );
        printf("Age range 40-60 years: %d cases\n",fourty );
        printf("Age range 60+ years: %d cases\n",sixty );
      }
    }

    //printf("%d\n",country_num );


  }

}


char* make_txt_name(date* date)
{
  char name_txt[20];
  if(date->Day > 9 && date->Month > 9)
  {
    snprintf(name_txt,sizeof(name_txt),"%d-%d-%d.txt",date->Day,date->Month,date->Year);
  }
  else if(date->Day < 10 && date->Month > 9)
  {
    snprintf(name_txt,sizeof(name_txt),"0%d-%d-%d.txt",date->Day,date->Month,date->Year);
  }
  else if(date->Day > 9 && date->Month < 10)
  {
    snprintf(name_txt,sizeof(name_txt),"%d-0%d-%d.txt",date->Day,date->Month,date->Year);
  }
  else if(date->Day < 10 && date->Month < 10)
  {
    snprintf(name_txt,sizeof(name_txt),"0%d-0%d-%d.txt",date->Day,date->Month,date->Year);
  }
  char* ptr = name_txt;
  return ptr;

}

int option(char* buffer,int ** fd_array,int* pid_array,country_parent_list* list,int numWorkers,int requests_count)
{
  char tmp_buf[100];
  const char s[2] = " ";//for strtok
  const char t[2] = "\n";
  const char a[2] = "-";
  char *token;
  char* ptr = tmp_buf;
  strcpy(tmp_buf,buffer);
  token = strtok(tmp_buf, s);//take the first word
  if(!strcmp(tmp_buf,"/listCountries"))//FIRST CASEE
  {
    printf("case 1 \n" );

    country_parent_node* ptr = list ->head;

    while(ptr!=NULL)
    {
    //printf("%d is worker_id\n",ptr->worker_id );
    printf("%s ",ptr->country);
    printf("%d\n",pid_array[ptr->worker_id] );
    ptr = ptr->next;
    }

    return 1;
  }
  else if(!strcmp(tmp_buf,"/exit"))
  {
    printf("exiting\n" );
    for(int i = 0; i < numWorkers;i++)//send exit option to all workers
    {
      kill(pid_array[i],SIGKILL);
      make_parent_log(list,requests_count);
      // write(fd_array[i][0],"7",4);
      // read(fd_array[i][1],tmp_buf,4);//wait worker  to finish
    }
    return 0;
  }
  else if(!strcmp(tmp_buf,"/diseaseFrequency"))
  {
    date* date1,*date2;
    date1 = (date*)malloc(sizeof(date));
    date2 = (date*)malloc(sizeof(date));
    char cntry[20];
    char disease[20];
    int country_val =0;
    //take the disease
    ptr = buffer;
    while (ptr[0] != s[0])//skip ortion
    {
      ptr++;
    }
    while (ptr[0]==s[0])//skip the spaces
    {
      ptr++;
    }
    strcpy(tmp_buf,ptr);//leave out the option
    strcpy(buffer,tmp_buf);//leave out the option
    ptr = tmp_buf;
    token = strtok(tmp_buf,s);//now temp has only the disease
    strcpy(disease,token);

    //take the country or date
    ptr = buffer;
    while (ptr[0] != s[0])//skip disease
    {
      ptr++;
    }
    while (ptr[0]==s[0])//skip the spaces
    {
      ptr++;
    }
    strcpy(tmp_buf,ptr);//leave out the disease
    strcpy(buffer,tmp_buf);//leave out the disease
    ptr = tmp_buf;

    //take the date1 now
    token = strtok(ptr,a);//now temp has only the entryDate.date
    date1->Day = atoi(token);

    //Get month
    char*tmp;
    strncpy(tmp_buf, buffer, 20);//take the whole date again
    tmp = tmp_buf;
    while(tmp[0]!=a[0])//skip the Day
    {
      tmp++;
    }
    tmp++;
    token = strtok(tmp, a);
    date1->Month = atoi(token);
    //Get year
    strncpy(tmp_buf, buffer, 20);//take the whole date again
    tmp = tmp_buf;
    while(tmp[0]!=a[0])//skip the Day
    {
      tmp++;
    }
    tmp++;
    while(tmp[0]!=a[0])//skip the Month
    {
      tmp++;
    }
    tmp++;
    token = strtok(tmp, a);
    date1->Year = atoi(token);

    //take the  date2_____________________________________________
    ptr = buffer;
    while (ptr[0] != s[0])//skip date1
    {
      ptr++;
    }
    while (ptr[0]==s[0])//skip the spaces
    {
      ptr++;
    }
    strcpy(tmp_buf,ptr);//leave out the Date1
    strcpy(buffer,tmp_buf);//both have the data after the Date1
    ptr = tmp_buf;
    token = strtok(ptr,s);//now temp has only the Date2

    //Get day
    token = strtok(tmp_buf, a);
    date2->Day = atoi(token);
    //Get month
    strncpy(tmp_buf, buffer, 20);//take the whole date again
    tmp = tmp_buf;
    while(tmp[0]!=a[0])//skip the Day
    {
      tmp++;
    }
    tmp++;
    token = strtok(tmp, a);
    date2->Month = atoi(token);
    //Get year
    strncpy(tmp_buf, buffer, 20);//take the whole date again
    tmp = tmp_buf;
    while(tmp[0]!=a[0])//skip the Day
    {
      tmp++;
    }
    tmp++;
    while(tmp[0]!=a[0])//skip the Month
    {
      tmp++;
    }
    tmp++;
    token = strtok(tmp, a);
    date2->Year = atoi(token);

    ptr = tmp_buf;
    while (ptr[0] != s[0])//skip date2
    {
      ptr++;
    }
    while (ptr[0]==s[0])//skip the spaces
    {
      ptr++;
    }

    if(ptr[0]>65 && ptr[0]<122)//if there IS a character after the spaces so there is a country
    {
      //take the country
      token = strtok(ptr,s);//now temp has only the country
      strcpy(cntry,token);
      country_val=1;
    }
    // printf("%s %s\n",disease,cntry );
    // printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
    // printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );
    //call function
    if(country_val==0)
    {
      diseaseFrequency1(list ,disease,date1,date2,numWorkers,fd_array);


    }
    else
    {

      diseaseFrequency(list ,disease,cntry,date1,date2,numWorkers,fd_array);
    }

    free(date1);
    free(date2);
    return 1;
  }
  else if(!strcmp(tmp_buf,"/searchPatientRecord"))
  {
    char lol[2];
    //take the record_id
    ptr = buffer;
    while (ptr[0] != s[0])//skip option
    {
      ptr++;
    }
    while (ptr[0]==s[0])//skip the spaces
    {
      ptr++;
    }
    strcpy(tmp_buf,ptr);//leave out the option
    strcpy(buffer,tmp_buf);//leave out the option
    ptr = tmp_buf;
    token = strtok(tmp_buf,s);//now temp has only the rec_id
    //printf("----------%s\n",tmp_buf );

    for(int i = 0; i < numWorkers;i++)//send option to all workers
    {
      write(fd_array[i][0],"4",4);
      write(fd_array[i][0],tmp_buf,4);//send record id
      read(fd_array[i][1],lol,4);

    }


    return 1;

  }
  else if(!strcmp(tmp_buf,"/numPatientAdmissions"))
  {
      date* date1,*date2;
      date1 = (date*)malloc(sizeof(date));
      date2 = (date*)malloc(sizeof(date));
      char cntry[20];
      char disease[20];
      int country_val =0;
      //take the disease
      ptr = buffer;
      while (ptr[0] != s[0])//skip ortion
      {
        ptr++;
      }
      while (ptr[0]==s[0])//skip the spaces
      {
        ptr++;
      }
      strcpy(tmp_buf,ptr);//leave out the option
      strcpy(buffer,tmp_buf);//leave out the option
      ptr = tmp_buf;
      token = strtok(tmp_buf,s);//now temp has only the disease
      strcpy(disease,token);

      //take the country or date
      ptr = buffer;
      while (ptr[0] != s[0])//skip disease
      {
        ptr++;
      }
      while (ptr[0]==s[0])//skip the spaces
      {
        ptr++;
      }
      strcpy(tmp_buf,ptr);//leave out the disease
      strcpy(buffer,tmp_buf);//leave out the disease
      ptr = tmp_buf;

      //take the date1 now
      token = strtok(ptr,a);//now temp has only the entryDate.date
      date1->Day = atoi(token);

      //Get month
      char*tmp;
      strncpy(tmp_buf, buffer, 20);//take the whole date again
      tmp = tmp_buf;
      while(tmp[0]!=a[0])//skip the Day
      {
        tmp++;
      }
      tmp++;
      token = strtok(tmp, a);
      date1->Month = atoi(token);
      //Get year
      strncpy(tmp_buf, buffer, 20);//take the whole date again
      tmp = tmp_buf;
      while(tmp[0]!=a[0])//skip the Day
      {
        tmp++;
      }
      tmp++;
      while(tmp[0]!=a[0])//skip the Month
      {
        tmp++;
      }
      tmp++;
      token = strtok(tmp, a);
      date1->Year = atoi(token);

      //take the  date2_____________________________________________
      ptr = buffer;
      while (ptr[0] != s[0])//skip date1
      {
        ptr++;
      }
      while (ptr[0]==s[0])//skip the spaces
      {
        ptr++;
      }
      strcpy(tmp_buf,ptr);//leave out the Date1
      strcpy(buffer,tmp_buf);//both have the data after the Date1
      ptr = tmp_buf;
      token = strtok(ptr,s);//now temp has only the Date2

      //Get day
      token = strtok(tmp_buf, a);
      date2->Day = atoi(token);
      //Get month
      strncpy(tmp_buf, buffer, 20);//take the whole date again
      tmp = tmp_buf;
      while(tmp[0]!=a[0])//skip the Day
      {
        tmp++;
      }
      tmp++;
      token = strtok(tmp, a);
      date2->Month = atoi(token);
      //Get year
      strncpy(tmp_buf, buffer, 20);//take the whole date again
      tmp = tmp_buf;
      while(tmp[0]!=a[0])//skip the Day
      {
        tmp++;
      }
      tmp++;
      while(tmp[0]!=a[0])//skip the Month
      {
        tmp++;
      }
      tmp++;
      token = strtok(tmp, a);
      date2->Year = atoi(token);

      ptr = tmp_buf;
      while (ptr[0] != s[0])//skip date2
      {
        ptr++;
      }
      while (ptr[0]==s[0])//skip the spaces
      {
        ptr++;
      }

      if(ptr[0]>65 && ptr[0]<122)//if there IS a character after the spaces so there is a country
      {
        //take the country
        token = strtok(ptr,s);//now temp has only the country
        strcpy(cntry,token);
        country_val=1;
      }
      // printf("%s %s\n",disease,cntry );
      // printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
      // printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );
      //call function
      if(country_val==0)
      {
        numPatientAdmissions1(list ,disease,date1,date2,numWorkers,fd_array);
      }
      else
      {
        numPatientAdmissions(list ,disease,cntry,date1,date2,numWorkers,fd_array);
      }

      free(date1);
      free(date2);
      return 1;
  }
  else if(!strcmp(tmp_buf,"/numPatientDischarges"))
  {
      date* date1,*date2;
      date1 = (date*)malloc(sizeof(date));
      date2 = (date*)malloc(sizeof(date));
      char cntry[20];
      char disease[20];
      int country_val =0;
      //take the disease
      ptr = buffer;
      while (ptr[0] != s[0])//skip ortion
      {
        ptr++;
      }
      while (ptr[0]==s[0])//skip the spaces
      {
        ptr++;
      }
      strcpy(tmp_buf,ptr);//leave out the option
      strcpy(buffer,tmp_buf);//leave out the option
      ptr = tmp_buf;
      token = strtok(tmp_buf,s);//now temp has only the disease
      strcpy(disease,token);

      //take the country or date
      ptr = buffer;
      while (ptr[0] != s[0])//skip disease
      {
        ptr++;
      }
      while (ptr[0]==s[0])//skip the spaces
      {
        ptr++;
      }
      strcpy(tmp_buf,ptr);//leave out the disease
      strcpy(buffer,tmp_buf);//leave out the disease
      ptr = tmp_buf;

      //take the date1 now
      token = strtok(ptr,a);//now temp has only the entryDate.date
      date1->Day = atoi(token);

      //Get month
      char*tmp;
      strncpy(tmp_buf, buffer, 20);//take the whole date again
      tmp = tmp_buf;
      while(tmp[0]!=a[0])//skip the Day
      {
        tmp++;
      }
      tmp++;
      token = strtok(tmp, a);
      date1->Month = atoi(token);
      //Get year
      strncpy(tmp_buf, buffer, 20);//take the whole date again
      tmp = tmp_buf;
      while(tmp[0]!=a[0])//skip the Day
      {
        tmp++;
      }
      tmp++;
      while(tmp[0]!=a[0])//skip the Month
      {
        tmp++;
      }
      tmp++;
      token = strtok(tmp, a);
      date1->Year = atoi(token);

      //take the  date2_____________________________________________
      ptr = buffer;
      while (ptr[0] != s[0])//skip date1
      {
        ptr++;
      }
      while (ptr[0]==s[0])//skip the spaces
      {
        ptr++;
      }
      strcpy(tmp_buf,ptr);//leave out the Date1
      strcpy(buffer,tmp_buf);//both have the data after the Date1
      ptr = tmp_buf;
      token = strtok(ptr,s);//now temp has only the Date2

      //Get day
      token = strtok(tmp_buf, a);
      date2->Day = atoi(token);
      //Get month
      strncpy(tmp_buf, buffer, 20);//take the whole date again
      tmp = tmp_buf;
      while(tmp[0]!=a[0])//skip the Day
      {
        tmp++;
      }
      tmp++;
      token = strtok(tmp, a);
      date2->Month = atoi(token);
      //Get year
      strncpy(tmp_buf, buffer, 20);//take the whole date again
      tmp = tmp_buf;
      while(tmp[0]!=a[0])//skip the Day
      {
        tmp++;
      }
      tmp++;
      while(tmp[0]!=a[0])//skip the Month
      {
        tmp++;
      }
      tmp++;
      token = strtok(tmp, a);
      date2->Year = atoi(token);

      ptr = tmp_buf;
      while (ptr[0] != s[0])//skip date2
      {
        ptr++;
      }
      while (ptr[0]==s[0])//skip the spaces
      {
        ptr++;
      }

      if(ptr[0]>65 && ptr[0]<122)//if there IS a character after the spaces so there is a country
      {
        //take the country
        token = strtok(ptr,s);//now temp has only the country
        strcpy(cntry,token);
        country_val=1;
      }
      // printf("%s %s\n",disease,cntry );
      // printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
      // printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );
      //call function
      if(country_val==0)
      {
        numPatientDischarges1(list ,disease,date1,date2,numWorkers,fd_array);
      }
      else
      {
        numPatientDischarges(list ,disease,cntry,date1,date2,numWorkers,fd_array);
      }

      free(date1);
      free(date2);
      return 1;
  }
  else if(!strcmp(tmp_buf,"/topk-AgeRanges"))
  {
    date* date1,*date2;
    date1 = (date*)malloc(sizeof(date));
    date2 = (date*)malloc(sizeof(date));
    char cntry[20];
    char disease[20];
    int country_val =0;
    int k;

    //take the k
    ptr = buffer;
    while (ptr[0] != s[0])//skip ortion
    {
      ptr++;
    }
    while (ptr[0]==s[0])//skip the spaces
    {
      ptr++;
    }
    strcpy(tmp_buf,ptr);//leave out the option
    strcpy(buffer,tmp_buf);//leave out the option
    ptr = tmp_buf;
    token = strtok(tmp_buf,s);//now temp has only the k
    k = atoi(tmp_buf);

    //take the country
    ptr = buffer;
    while (ptr[0] != s[0])//skip ortion
    {
      ptr++;
    }
    while (ptr[0]==s[0])//skip the spaces
    {
      ptr++;
    }
    strcpy(tmp_buf,ptr);//leave out the k
    strcpy(buffer,tmp_buf);//leave out the k
    ptr = tmp_buf;
    token = strtok(tmp_buf,s);//now temp has only the country
    strcpy(cntry,token);

    //take the disease
    ptr = buffer;
    while (ptr[0] != s[0])//skip ortion
    {
      ptr++;
    }
    while (ptr[0]==s[0])//skip the spaces
    {
      ptr++;
    }
    strcpy(tmp_buf,ptr);//leave out the country
    strcpy(buffer,tmp_buf);//leave out the k
    ptr = tmp_buf;
    token = strtok(tmp_buf,s);//now temp has only the disease
    strcpy(disease,token);

    //take the date1
    ptr = buffer;
    while (ptr[0] != s[0])//skip disease
    {
      ptr++;
    }
    while (ptr[0]==s[0])//skip the disease
    {
      ptr++;
    }
    strcpy(tmp_buf,ptr);//leave out the disease
    strcpy(buffer,tmp_buf);//leave out the disease
    ptr = tmp_buf;
    //take the date1 now
    token = strtok(ptr,a);//now temp has only the entryDate.date
    date1->Day = atoi(token);

    //Get month
    char*tmp;
    strncpy(tmp_buf, buffer, 20);//take the whole date again
    tmp = tmp_buf;
    while(tmp[0]!=a[0])//skip the Day
    {
      tmp++;
    }
    tmp++;
    token = strtok(tmp, a);
    date1->Month = atoi(token);
    //Get year
    strncpy(tmp_buf, buffer, 20);//take the whole date again
    tmp = tmp_buf;
    while(tmp[0]!=a[0])//skip the Day
    {
      tmp++;
    }
    tmp++;
    while(tmp[0]!=a[0])//skip the Month
    {
      tmp++;
    }
    tmp++;
    token = strtok(tmp, a);
    date1->Year = atoi(token);

    //take the  date2_____________________________________________
    ptr = buffer;
    while (ptr[0] != s[0])//skip date1
    {
      ptr++;
    }
    while (ptr[0]==s[0])//skip the spaces
    {
      ptr++;
    }
    strcpy(tmp_buf,ptr);//leave out the Date1
    strcpy(buffer,tmp_buf);//both have the data after the Date1
    ptr = tmp_buf;
    token = strtok(ptr,s);//now temp has only the Date2

    //Get day
    token = strtok(tmp_buf, a);
    date2->Day = atoi(token);
    //Get month
    strncpy(tmp_buf, buffer, 20);//take the whole date again
    tmp = tmp_buf;
    while(tmp[0]!=a[0])//skip the Day
    {
      tmp++;
    }
    tmp++;
    token = strtok(tmp, a);
    date2->Month = atoi(token);
    //Get year
    strncpy(tmp_buf, buffer, 20);//take the whole date again
    tmp = tmp_buf;
    while(tmp[0]!=a[0])//skip the Day
    {
      tmp++;
    }
    tmp++;
    while(tmp[0]!=a[0])//skip the Month
    {
      tmp++;
    }
    tmp++;
    token = strtok(tmp, a);
    date2->Year = atoi(token);
    // printf("%d %s %s\n",k,disease,cntry );
    // printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
    // printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );
    topkAgeRanges(list ,disease,cntry,date1,date2,numWorkers,fd_array,k);

    free(date1);
    free(date2);
    return 1;


  }
  else
  {
    printf("Maybe you made a typo.Try again\n" );
    return 1;
  }

}

int worker_wait(int readfd,country_list_node* country_list,int writefd)
{

  country_list_node* list = country_list;
  char temp[20];
  char disease[20];
  int country_num;
  date* date1,*date2;
  date1 = (date*)malloc(sizeof(date));
  date2 = (date*)malloc(sizeof(date));


  read(readfd,temp,4);//read the option
  int  option_case = atoi(temp);
  //printf("option case %d\n",option_case);

  if(option_case==2)
  {
    //read disease
    read(readfd,temp,10);
    strcpy(disease,temp);
    //printf("%s\n",disease );

    //read dates
    read(readfd,temp,4);
    date1->Day = atoi(temp);
    read(readfd,temp,4);
    date1->Month = atoi(temp);
    read(readfd,temp,4);
    date1->Year = atoi(temp);

    read(readfd,temp,4);
    date2->Day = atoi(temp);
    read(readfd,temp,4);
    date2->Month = atoi(temp);
    read(readfd,temp,4);
    date2->Year = atoi(temp);
    //printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
    //printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );

    //read countr num
    read(readfd,temp,4);
    country_num = atoi(temp);
    //printf("---->%d\n",country_num );
    country_list_node* ptr = country_list;
    //find the country
    while(ptr!=NULL)
    {
      if(ptr->country_id==country_num)
      {
        //printf("%s\n",ptr->country);
        break;
      }
      ptr = ptr->next;
    }
    //now i have all the data i need
    rec_node* rec_ptr = ptr->rec_list->head;
    int counter = 0;
    int h;
    while(rec_ptr!=NULL)
    {
      if(!strcmp(disease,rec_ptr->diseaseID))//check disease
      {

        h = datecmp(rec_ptr->date,date2);//check dates
        if(h>0)
        {
          h = datecmp(rec_ptr->date,date1);
          if(h==0 || h==3)
          {
            //printf("%d-%d-%s-%s-%s-%d\n",rec_ptr->id,rec_ptr->enter_exit,rec_ptr->name,rec_ptr->last_name,rec_ptr->diseaseID,rec_ptr->age );
            counter++;
          }
        }

      }
      rec_ptr = rec_ptr->next;
    }
    printf("%d \n",counter );

    free(date1);
    free(date2);
    write(writefd,"1",4);//so parent will wait for child to finish
    return 1;

  }
  else if(option_case==21)
  {
    //same but without country
    //read disease
    read(readfd,temp,10);
    strcpy(disease,temp);
    //printf("%s\n",disease );

    //read dates
    read(readfd,temp,4);
    date1->Day = atoi(temp);
    read(readfd,temp,4);
    date1->Month = atoi(temp);
    read(readfd,temp,4);
    date1->Year = atoi(temp);

    read(readfd,temp,4);
    date2->Day = atoi(temp);
    read(readfd,temp,4);
    date2->Month = atoi(temp);
    read(readfd,temp,4);
    date2->Year = atoi(temp);
    // printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
    // printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );

    //for every country
    country_list_node* ptr = country_list;
    int counter = 0;
    int h;
    rec_node* rec_ptr;
    while(ptr!=NULL)//country loop
    {
      rec_ptr = ptr->rec_list->head;

      while(rec_ptr!=NULL)
      {
        if(!strcmp(disease,rec_ptr->diseaseID))//check disease
        {

          h = datecmp(rec_ptr->date,date2);//check dates
          if(h>0)
          {
            h = datecmp(rec_ptr->date,date1);
            if(h==0 || h==3)
            {
              //printf("%s %d-%d-%s-%s-%s-%d\n",ptr->country,rec_ptr->id,rec_ptr->enter_exit,rec_ptr->name,rec_ptr->last_name,rec_ptr->diseaseID,rec_ptr->age );
              counter++;
            }
          }

        }
        rec_ptr = rec_ptr->next;
      }
      ptr = ptr->next;
    }

    //sent count to parent
    snprintf(temp,sizeof(temp),"%d",counter);
    write(writefd,temp,4);

    free(date1);
    free(date2);
    return 1;



  }
  else if(option_case==7)//exit
  {
    // write(writefd,"1",4);
    // free(date1);
    // free(date2);
    return 0 ;
  }
  else if(option_case==4)
  {
    int rec_id;
    read(readfd,temp,4);
    rec_id = atoi(temp);
    //printf("worker->recid:%s\n",temp);

    country_list_node* ptr = country_list;
    rec_node* rec_ptr;
    rec_node* search_exit;
    int exit,flag = 0;
    while(ptr!=NULL)//for every country the worker has
    {
      rec_ptr = ptr->rec_list->head;
      while(rec_ptr!=NULL)//search every record
      {
        if(rec_id==rec_ptr->id && rec_ptr->enter_exit==0)//if its an enter record
        {
          //print
          printf("%d %s %s %s %d",rec_ptr->id,rec_ptr->name,rec_ptr->last_name,rec_ptr->diseaseID,rec_ptr->age );
          printf("%d-%d-%d ",rec_ptr->date->Day,rec_ptr->date->Month,rec_ptr->date->Year);
          //search for exit
          search_exit = rec_ptr->next;
          while(search_exit!=NULL)
          {
            if(search_exit->id ==rec_id)//if we found same id means it has an exit date
            {
              printf("%d-%d-%d ",search_exit->date->Day,search_exit->date->Month,search_exit->date->Year);
              flag = 1;
            }
            search_exit=search_exit->next;
          }
          if(flag==0)
          {
            printf("--\n" );
          }


        }
        rec_ptr = rec_ptr->next;
      }
      ptr = ptr->next;
    }
    write(writefd,"1",4);
    return 1;
  }
  else if(option_case == 5)
  {
    //read disease
    read(readfd,temp,10);
    strcpy(disease,temp);
    //printf("%s\n",disease );

    //read dates
    read(readfd,temp,4);
    date1->Day = atoi(temp);
    read(readfd,temp,4);
    date1->Month = atoi(temp);
    read(readfd,temp,4);
    date1->Year = atoi(temp);

    read(readfd,temp,4);
    date2->Day = atoi(temp);
    read(readfd,temp,4);
    date2->Month = atoi(temp);
    read(readfd,temp,4);
    date2->Year = atoi(temp);
    //printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
    //printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );

    //read countr num
    read(readfd,temp,4);
    country_num = atoi(temp);
    //printf("---->%d\n",country_num );
    country_list_node* ptr = country_list;
    //find the country
    while(ptr!=NULL)
    {
      if(ptr->country_id==country_num)
      {
        //printf("%s\n",ptr->country);
        break;
      }
      ptr = ptr->next;
    }
    //now i have all the data i need
    rec_node* rec_ptr = ptr->rec_list->head;
    int counter = 0;
    int h;
    while(rec_ptr!=NULL)
    {
      if(!strcmp(disease,rec_ptr->diseaseID))//check disease
      {

        h = datecmp(rec_ptr->date,date2);//check dates
        if(h>0)
        {
          h = datecmp(rec_ptr->date,date1);
          if(h==0 || h==3)
          {
            if(rec_ptr->enter_exit==0)
            counter++;
            //printf("%d-%d-%s-%s-%s-%d\n",rec_ptr->id,rec_ptr->enter_exit,rec_ptr->name,rec_ptr->last_name,rec_ptr->diseaseID,rec_ptr->age );

          }
        }

      }
      rec_ptr = rec_ptr->next;
    }
    printf("%d \n",counter );

    write(writefd,"1",4);
    free(date1);
    free(date2);
    return 1;
  }
  else if(option_case == 51)
  {
    //same but without country
    //read disease
    read(readfd,temp,10);
    strcpy(disease,temp);
    //printf("%s\n",disease );

    //read dates
    read(readfd,temp,4);
    date1->Day = atoi(temp);
    read(readfd,temp,4);
    date1->Month = atoi(temp);
    read(readfd,temp,4);
    date1->Year = atoi(temp);

    read(readfd,temp,4);
    date2->Day = atoi(temp);
    read(readfd,temp,4);
    date2->Month = atoi(temp);
    read(readfd,temp,4);
    date2->Year = atoi(temp);
    // printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
    // printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );

    //for every country
    country_list_node* ptr = country_list;
    int counter = 0;
    int h;
    rec_node* rec_ptr;
    while(ptr!=NULL)//country loop
    {
      counter = 0;
      rec_ptr = ptr->rec_list->head;

      while(rec_ptr!=NULL)
      {
        if(!strcmp(disease,rec_ptr->diseaseID))//check disease
        {

          h = datecmp(rec_ptr->date,date2);//check dates
          if(h>0)
          {
            h = datecmp(rec_ptr->date,date1);
            if(h==0 || h==3)
            {
              if(rec_ptr->enter_exit==0)
              counter++;
              //printf("%s %d-%d-%s-%s-%s-%d\n",ptr->country,rec_ptr->id,rec_ptr->enter_exit,rec_ptr->name,rec_ptr->last_name,rec_ptr->diseaseID,rec_ptr->age );

            }
          }

        }
        rec_ptr = rec_ptr->next;
      }
      //print answer and country
      printf("%s %d\n",ptr->country,counter);

      ptr = ptr->next;
    }

    write(writefd,"1",4);
    free(date1);
    free(date2);
    return 1;

  }
  else if(option_case == 6)
  {
    //read disease
    read(readfd,temp,10);
    strcpy(disease,temp);
    //printf("%s\n",disease );
    //read dates
    read(readfd,temp,4);
    date1->Day = atoi(temp);
    read(readfd,temp,4);
    date1->Month = atoi(temp);
    read(readfd,temp,4);
    date1->Year = atoi(temp);
    read(readfd,temp,4);
    date2->Day = atoi(temp);
    read(readfd,temp,4);
    date2->Month = atoi(temp);
    read(readfd,temp,4);
    date2->Year = atoi(temp);
    //printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
    //printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );
    //read countr num
    read(readfd,temp,4);
    country_num = atoi(temp);
    //printf("---->%d\n",country_num );
    country_list_node* ptr = country_list;
    //find the country
    while(ptr!=NULL)
    {
      if(ptr->country_id==country_num)
      {
        //printf("%s\n",ptr->country);
        break;
      }
      ptr = ptr->next;
    }
    //now i have all the data i need
    rec_node* rec_ptr = ptr->rec_list->head;
    int counter = 0;
    int h;
    while(rec_ptr!=NULL)
    {
      if(!strcmp(disease,rec_ptr->diseaseID))//check disease
      {

        h = datecmp(rec_ptr->date,date2);//check dates
        if(h>0)
        {
          h = datecmp(rec_ptr->date,date1);
          if(h==0 || h==3)
          {
            if(rec_ptr->enter_exit==1)
            counter++;
            //printf("%d-%d-%s-%s-%s-%d\n",rec_ptr->id,rec_ptr->enter_exit,rec_ptr->name,rec_ptr->last_name,rec_ptr->diseaseID,rec_ptr->age );

          }
        }

      }
      rec_ptr = rec_ptr->next;
    }
    printf("i counted %d EXIT cases \n",counter );

    free(date1);
    free(date2);
    write(writefd,"1",4);
    return 1;
  }
  else if(option_case == 61)
  {
    //same but without country
    //read disease
    read(readfd,temp,10);
    strcpy(disease,temp);
    //printf("%s\n",disease );

    //read dates
    read(readfd,temp,4);
    date1->Day = atoi(temp);
    read(readfd,temp,4);
    date1->Month = atoi(temp);
    read(readfd,temp,4);
    date1->Year = atoi(temp);

    read(readfd,temp,4);
    date2->Day = atoi(temp);
    read(readfd,temp,4);
    date2->Month = atoi(temp);
    read(readfd,temp,4);
    date2->Year = atoi(temp);
    // printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
    // printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );

    //for every country
    country_list_node* ptr = country_list;
    int counter = 0;
    int h;
    rec_node* rec_ptr;
    while(ptr!=NULL)//country loop
    {
      counter = 0;
      rec_ptr = ptr->rec_list->head;

      while(rec_ptr!=NULL)
      {
        if(!strcmp(disease,rec_ptr->diseaseID))//check disease
        {

          h = datecmp(rec_ptr->date,date2);//check dates
          if(h>0)
          {
            h = datecmp(rec_ptr->date,date1);
            if(h==0 || h==3)
            {
              if(rec_ptr->enter_exit==1)
              counter++;
              //printf("%s %d-%d-%s-%s-%s-%d\n",ptr->country,rec_ptr->id,rec_ptr->enter_exit,rec_ptr->name,rec_ptr->last_name,rec_ptr->diseaseID,rec_ptr->age );

            }
          }

        }
        rec_ptr = rec_ptr->next;
      }
      //print answer and country
      printf("%s %d\n",ptr->country,counter);

      ptr = ptr->next;
    }


    free(date1);
    free(date2);
    write(writefd,"1",4);
    return 1;

  }
  else if(option_case == 3)
  {
    int k;
    //read disease
    read(readfd,temp,10);
    strcpy(disease,temp);
    //read dates
    read(readfd,temp,4);
    date1->Day = atoi(temp);
    read(readfd,temp,4);
    date1->Month = atoi(temp);
    read(readfd,temp,4);
    date1->Year = atoi(temp);
    read(readfd,temp,4);
    date2->Day = atoi(temp);
    read(readfd,temp,4);
    date2->Month = atoi(temp);
    read(readfd,temp,4);
    date2->Year = atoi(temp);
    // printf("%s\n",disease );
    // printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
    // printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );
    //read countr num
    read(readfd,temp,4);
    country_num = atoi(temp);
    //read k
    read(readfd,temp,4);
    k = atoi(temp);
    // printf("%d is k \n",k );
    //printf("---->%d\n",country_num );

    country_list_node* ptr = country_list;
    //find the country
    while(ptr!=NULL)
    {
      if(ptr->country_id==country_num)
      {
        printf("%s\n",ptr->country);
        break;
      }
      ptr = ptr->next;
    }

    //nw i have everything i need
    rec_node* rec_ptr = ptr->rec_list->head;
    int counter = 0;
    int h,zero=0,twenty=0,fourty=0,sixty=0;
    while(rec_ptr!=NULL)
    {
      if(!strcmp(disease,rec_ptr->diseaseID))//check disease
      {

        h = datecmp(rec_ptr->date,date2);//check dates
        if(h>0)
        {
          h = datecmp(rec_ptr->date,date1);
          if(h==0 || h==3)
          {
            if(rec_ptr->age > 20 && rec_ptr->age <= 40)
            {
              twenty++;
            }
            else if(rec_ptr->age > 40 && rec_ptr->age <= 60)
            {
              fourty++;
            }
            else if(rec_ptr->age > 60)
            {
              sixty++;
            }
            else
            {
              zero++;
            }
            //printf("%d-%d-%s-%s-%s-%d\n",rec_ptr->id,rec_ptr->enter_exit,rec_ptr->name,rec_ptr->last_name,rec_ptr->diseaseID,rec_ptr->age );
          }
        }

      }
      rec_ptr = rec_ptr->next;
    }

    if(k>4)//there are onnly 4 age groups
    k=4;
    int total = zero+twenty+fourty+sixty;
    int a,b,c,d;
    a = (zero*100)/total;
    b = (twenty*100)/total;
    c = (fourty*100)/total;
    d = (sixty*100)/total;

    printf(" 0-20: %d%%\n 20-40: %d%%\n 40-60: %d%%\n 60+: %d%%\n",a,b,c,d);
    write(writefd,"1",4);

    return 1;

    topk_node* zero_node,*twenty_node,*fourty_node,*sixty_node;

    zero_node = malloc(sizeof(topk_node));
    twenty_node = malloc(sizeof(topk_node));
    fourty_node = malloc(sizeof(topk_node));
    sixty_node = malloc(sizeof(topk_node));

    zero_node->percent = (zero*100)/total;
    twenty_node->percent = (twenty*100)/total;
    fourty_node->percent = (fourty*100)/total;
    sixty_node->percent = (sixty*100)/total;

    zero_node->name = 0;
    twenty_node->name = 20;
    fourty_node->name = 40;
    sixty_node->name = 60;

    zero_node->count=zero;
    twenty_node->count=twenty;
    fourty_node->count = fourty;
    sixty_node->count =sixty;

    // int t_zero,t_twenty,t_fourty,t_sixty;
    topk_list* list;
    list = malloc(sizeof(topk_list));
    list->head = NULL;

    insert_topk_list(list,zero_node);
    insert_topk_list(list,twenty_node);
    insert_topk_list(list,fourty_node);
    insert_topk_list(list,sixty_node);

    //return 1;

    topk_node* pt = list->head;
    while (ptr!=NULL)
    {
      printf("older than%d has %d percent\n",pt->name,pt->percent );
      pt =pt->next;
      printf("ssss\n" );
    }


    printf("111111111\n");
    free(date1);
    free(date2);
    free(zero_node);
    free(twenty_node);
    free(fourty_node);
    free(sixty_node);
    free(list);
    printf("222222\n");
    return 1;


  }
  else
  {
    return 0;
  }

}



void topkAgeRanges(country_parent_list* list ,char* disease,char* cntry,date* date1,date* date2,int numWorkers,int ** fd_array,int k)
{
  char temp[20];
  country_parent_node* ptr = list->head;

  //find country at list
  while(ptr!=NULL)
  {
    if(!strcmp(ptr->country,cntry))
    {
      //printf("country num %d\n",ptr->country_num );
      //send code to that ONE worker
      write(fd_array[ptr->worker_id][0],"3",4); //2 is code for diseaseFrequency
      // printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
      // printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );

      //send disease
      write(fd_array[ptr->worker_id][0],disease,10);
      //send dates
      snprintf(temp,sizeof(temp),"%d",date1->Day);
      write(fd_array[ptr->worker_id][0],temp,4);
      snprintf(temp,sizeof(temp),"%d",date1->Month);
      write(fd_array[ptr->worker_id][0],temp,4);
      snprintf(temp,sizeof(temp),"%d",date1->Year);
      write(fd_array[ptr->worker_id][0],temp,4);

      snprintf(temp,sizeof(temp),"%d",date2->Day);
      write(fd_array[ptr->worker_id][0],temp,4);
      snprintf(temp,sizeof(temp),"%d",date2->Month);
      write(fd_array[ptr->worker_id][0],temp,4);
      snprintf(temp,sizeof(temp),"%d",date2->Year);
      write(fd_array[ptr->worker_id][0],temp,4);

      //send country num
      snprintf(temp,sizeof(temp),"%d",ptr->country_num);
      write(fd_array[ptr->worker_id][0],temp,4);

      //send k
      snprintf(temp,sizeof(temp),"%d",k);
      write(fd_array[ptr->worker_id][0],temp,4);

      read(fd_array[ptr->worker_id][1],temp,4);//wait worker  to finish

    }

    ptr = ptr->next;
  }


}
void diseaseFrequency(country_parent_list* list ,char* disease,char* cntry,date* date1,date* date2,int numWorkers,int ** fd_array)
{
  char temp[20];
  country_parent_node* ptr = list->head;

  //find country at list
  while(ptr!=NULL)
  {
    if(!strcmp(ptr->country,cntry))
    {
      //printf("country num %d\n",ptr->country_num );
      //send code to that ONE worker
      write(fd_array[ptr->worker_id][0],"2",4); //2 is code for diseaseFrequency
      // printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
      // printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );

      //send disease
      write(fd_array[ptr->worker_id][0],disease,10);
      //send dates
      snprintf(temp,sizeof(temp),"%d",date1->Day);
      write(fd_array[ptr->worker_id][0],temp,4);
      snprintf(temp,sizeof(temp),"%d",date1->Month);
      write(fd_array[ptr->worker_id][0],temp,4);
      snprintf(temp,sizeof(temp),"%d",date1->Year);
      write(fd_array[ptr->worker_id][0],temp,4);

      snprintf(temp,sizeof(temp),"%d",date2->Day);
      write(fd_array[ptr->worker_id][0],temp,4);
      snprintf(temp,sizeof(temp),"%d",date2->Month);
      write(fd_array[ptr->worker_id][0],temp,4);
      snprintf(temp,sizeof(temp),"%d",date2->Year);
      write(fd_array[ptr->worker_id][0],temp,4);

      //send country num
      snprintf(temp,sizeof(temp),"%d",ptr->country_num);
      write(fd_array[ptr->worker_id][0],temp,4);
      read(fd_array[ptr->worker_id][1],temp,4);//wait for worker
    }

    ptr = ptr->next;
  }
  // read(fd_array[ptr->worker_id][1],temp,4);//wait for worker
  // printf("%s\n",temp);
}

void diseaseFrequency1(country_parent_list* list ,char* disease,date* date1,date* date2,int numWorkers,int ** fd_array)
{

  char temp[20];
  //send to all workers and wait for reply

  //send code to  ALL worker
  for(int i= 0; i<numWorkers; i++)
  {

    write(fd_array[i][0],"21",4); //2 is code for diseaseFrequency1
    // printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
    // printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );
    //send disease
    write(fd_array[i][0],disease,10);
    //send dates
    snprintf(temp,sizeof(temp),"%d",date1->Day);
    write(fd_array[i][0],temp,4);
    snprintf(temp,sizeof(temp),"%d",date1->Month);
    write(fd_array[i][0],temp,4);
    snprintf(temp,sizeof(temp),"%d",date1->Year);
    write(fd_array[i][0],temp,4);

    snprintf(temp,sizeof(temp),"%d",date2->Day);
    write(fd_array[i][0],temp,4);
    snprintf(temp,sizeof(temp),"%d",date2->Month);
    write(fd_array[i][0],temp,4);
    snprintf(temp,sizeof(temp),"%d",date2->Year);
    write(fd_array[i][0],temp,4);


  }

  int cases_counter = 0;
  int b;
  for(int i= 0; i<numWorkers; i++)
  {
    //wait reply
    read(fd_array[i][1],temp,4);
    b = atoi(temp);
    cases_counter = cases_counter +b;
  }

  printf("cases found: %d\n",cases_counter );

}

void numPatientAdmissions1(country_parent_list* list ,char* disease,date* date1,date* date2,int numWorkers,int ** fd_array)
{
  char temp[20];
  //send to all workers and wait for reply

  //send code to  ALL worker
  for(int i= 0; i<numWorkers; i++)
  {

    write(fd_array[i][0],"51",4); //2 is code for diseaseFrequency1
    // printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
    // printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );
    //send disease
    write(fd_array[i][0],disease,10);
    //send dates
    snprintf(temp,sizeof(temp),"%d",date1->Day);
    write(fd_array[i][0],temp,4);
    snprintf(temp,sizeof(temp),"%d",date1->Month);
    write(fd_array[i][0],temp,4);
    snprintf(temp,sizeof(temp),"%d",date1->Year);
    write(fd_array[i][0],temp,4);

    snprintf(temp,sizeof(temp),"%d",date2->Day);
    write(fd_array[i][0],temp,4);
    snprintf(temp,sizeof(temp),"%d",date2->Month);
    write(fd_array[i][0],temp,4);
    snprintf(temp,sizeof(temp),"%d",date2->Year);
    write(fd_array[i][0],temp,4);
    read(fd_array[i][1],temp,4);//wait for worker


  }
}

void numPatientAdmissions(country_parent_list* list ,char* disease,char* cntry,date* date1,date* date2,int numWorkers,int ** fd_array)
{
  char temp[20];
  country_parent_node* ptr = list->head;

  //find country at list
  while(ptr!=NULL)
  {
    if(!strcmp(ptr->country,cntry))
    {
      //printf("country num %d\n",ptr->country_num );
      //send code to that ONE worker
      write(fd_array[ptr->worker_id][0],"5",4); //2 is code for diseaseFrequency
      // printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
      // printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );

      //send disease
      write(fd_array[ptr->worker_id][0],disease,10);
      //send dates
      snprintf(temp,sizeof(temp),"%d",date1->Day);
      write(fd_array[ptr->worker_id][0],temp,4);
      snprintf(temp,sizeof(temp),"%d",date1->Month);
      write(fd_array[ptr->worker_id][0],temp,4);
      snprintf(temp,sizeof(temp),"%d",date1->Year);
      write(fd_array[ptr->worker_id][0],temp,4);

      snprintf(temp,sizeof(temp),"%d",date2->Day);
      write(fd_array[ptr->worker_id][0],temp,4);
      snprintf(temp,sizeof(temp),"%d",date2->Month);
      write(fd_array[ptr->worker_id][0],temp,4);
      snprintf(temp,sizeof(temp),"%d",date2->Year);
      write(fd_array[ptr->worker_id][0],temp,4);

      //send country num
      snprintf(temp,sizeof(temp),"%d",ptr->country_num);
      write(fd_array[ptr->worker_id][0],temp,4);
      read(fd_array[ptr->worker_id][1],temp,4);//wait for worker

    }

    ptr = ptr->next;
  }

}

void numPatientDischarges1(country_parent_list* list ,char* disease,date* date1,date* date2,int numWorkers,int ** fd_array)
{
  char temp[20];
  //send to all workers and wait for reply

  //send code to  ALL worker
  for(int i= 0; i<numWorkers; i++)
  {

    write(fd_array[i][0],"61",4); //2 is code for diseaseFrequency1
    // printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
    // printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );
    //send disease
    write(fd_array[i][0],disease,10);
    //send dates
    snprintf(temp,sizeof(temp),"%d",date1->Day);
    write(fd_array[i][0],temp,4);
    snprintf(temp,sizeof(temp),"%d",date1->Month);
    write(fd_array[i][0],temp,4);
    snprintf(temp,sizeof(temp),"%d",date1->Year);
    write(fd_array[i][0],temp,4);

    snprintf(temp,sizeof(temp),"%d",date2->Day);
    write(fd_array[i][0],temp,4);
    snprintf(temp,sizeof(temp),"%d",date2->Month);
    write(fd_array[i][0],temp,4);
    snprintf(temp,sizeof(temp),"%d",date2->Year);
    write(fd_array[i][0],temp,4);
    read(fd_array[i][1],temp,4);//wait for worker

  }
}

void numPatientDischarges(country_parent_list* list ,char* disease,char* cntry,date* date1,date* date2,int numWorkers,int ** fd_array)
{
  char temp[20];
  country_parent_node* ptr = list->head;

  //find country at list
  while(ptr!=NULL)
  {
    if(!strcmp(ptr->country,cntry))
    {
      //printf("country num %d\n",ptr->country_num );
      //send code to that ONE worker
      write(fd_array[ptr->worker_id][0],"6",4); //2 is code for diseaseFrequency
      // printf("%d-%d-%d\n",date1->Day,date1->Month,date1->Year );
      // printf("%d-%d-%d\n",date2->Day,date2->Month,date2->Year );

      //send disease
      write(fd_array[ptr->worker_id][0],disease,10);
      //send dates
      snprintf(temp,sizeof(temp),"%d",date1->Day);
      write(fd_array[ptr->worker_id][0],temp,4);
      snprintf(temp,sizeof(temp),"%d",date1->Month);
      write(fd_array[ptr->worker_id][0],temp,4);
      snprintf(temp,sizeof(temp),"%d",date1->Year);
      write(fd_array[ptr->worker_id][0],temp,4);

      snprintf(temp,sizeof(temp),"%d",date2->Day);
      write(fd_array[ptr->worker_id][0],temp,4);
      snprintf(temp,sizeof(temp),"%d",date2->Month);
      write(fd_array[ptr->worker_id][0],temp,4);
      snprintf(temp,sizeof(temp),"%d",date2->Year);
      write(fd_array[ptr->worker_id][0],temp,4);

      //send country num
      snprintf(temp,sizeof(temp),"%d",ptr->country_num);
      write(fd_array[ptr->worker_id][0],temp,4);
      read(fd_array[ptr->worker_id][1],temp,4);//wait for worker

    }

    ptr = ptr->next;
  }

}

int insert_topk_list(topk_list* list,topk_node* new)
{
  topk_node* ptr = list->head;
  if(list->head==NULL)
  {
    list->head = new;
    new->next = NULL;
    return 0;
  }
  else
  {
    topk_node* prev = ptr;
    if(new->count >= prev->count)
    {
      printf("in if\n" );
      list->head = new;
      new->next = prev;
      return 0;
    }
    ptr = ptr->next;
    while(ptr!=NULL)
    {
      if(ptr->count <= new->count && prev->count > new->count)
      {
        //put between
        new->next = prev->next;
        prev->next = new;
        return 0;
      }
      prev = ptr;
      ptr = ptr->next;
    }
    //put at the end
    prev->next = new;
    new->next = NULL;

  }
  return 0;
}



void make_logfile(country_list_node* countries,int requests_count)
{
  int fail;
  country_list_node* ptr = countries;
  FILE *fp = NULL;
        int pid = getpid();
        fail = 0;
        char txt_name[50];
        snprintf(txt_name,sizeof(txt_name),"log_file.%d",pid);
        fp = fopen(txt_name ,"w");
        while (ptr!=NULL)
        {
          fprintf(fp, "%s\n",ptr->country);
          ptr = ptr->next;
        }
        fprintf(fp, "TOTAL %d\n",requests_count);
        fprintf(fp, "SUCCESS %d\n",requests_count);
        fprintf(fp, "FAIL %d\n",fail);



}

void make_parent_log(country_parent_list* list,int requests_count)
{
  country_parent_node* ptr = list->head;
  int fail;
  FILE *fp = NULL;
  int pid = getpid();
  fail = 0;
  char txt_name[50];
  snprintf(txt_name,sizeof(txt_name),"log_file.%d",pid);
  fp = fopen(txt_name ,"w");
  while (ptr!=NULL)
  {
    fprintf(fp, "%s\n",ptr->country);
    ptr = ptr->next;
  }
  fprintf(fp, "TOTAL %d\n",requests_count);
  fprintf(fp, "SUCCESS %d\n",requests_count);
  fprintf(fp, "FAIL %d\n",fail);
}
