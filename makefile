OBJS    = master.o functions.o
OBJS1    = Worker.o functions.o	functions2.o
OBJS2		 = whoServer.o functions.o	functions2.o
OBJS3 	 = whoClient.o 	functions.o	functions2.o
SOURCE    = master.c functions.c
SOURCE1    = Worker.c functions.c	functions2.c
SOURCE2		 = whoServer.c functions.c	functions2.c
SOURCE3		 = whoClient.c functions.c	functions2.c
HEADER    = functions.h	functions2.h
OUT    = master
OUT1    = Worker
OUT2 	  = whoServer
OUT3 	  = whoClient
CC    = gcc
FLAGS    = -g3 -c -pthread

all: $(OBJS) $(OBJS1)	$(OBJS2)	$(OBJS3)
		$(CC) -g3 $(OBJS) -o $(OUT)
		$(CC) -g3 $(OBJS1) -o $(OUT1)
		$(CC) -g3	-pthread $(OBJS2) -o $(OUT2)
		$(CC) -g3 -pthread $(OBJS3) -o $(OUT3)


master.o: master.c
		$(CC) $(FLAGS) master.c

worker.o: Worker.c
		$(CC) $(FLAGS) Worker.c

functions.o: functions.c
		$(CC) $(FLAGS) functions.c

functions2.o: functions2.c
		$(CC) $(FLAGS) functions2.c

whoServer.o:	whoServer.c
		$(CC) $(FLAGS) whoServer.c

whoClient.o:	whoClient.c
		$(CC) $(FLAGS) whoClient.c

clean:
		rm -f $(OBJS) $(OUT) $(OBJS1)	$(OUT1) $(OBJS2)	$(OBJS3)	$(OUT2)	$(OUT3)
