#ifndef FUNCTIONS2
#define FUNCTIONS2

struct sockaddr;
typedef struct arg_struct
{
  int bufferSize;
  pthread_cond_t* cond;
  pthread_mutex_t* lock;

}arg_struct;

typedef struct circular_buf_node
{
  int fd;
  int used;
  int task; //0 for worker 1 for client
  struct circular_buf_node * next;

}circular_buf_node;


typedef struct circular_buf_list
{
    circular_buf_node *head;
    circular_buf_node *tail;
    char* worker_ip;
    int empty; //0 for NOT empty 1 for empty
    int full; //0 for NOT full 1 for full
}circular_buf_list;


typedef struct worker_list_node
{
    int port;
    struct worker_list_node* next;

}worker_list_node;

typedef struct worker_list
{
    worker_list_node *head;
    worker_list_node *tail;

}worker_list;


typedef struct client_arg
{
  int servPort;
  char* line;
  const char* servIP;
}client_arg;



typedef struct server_arg
{
  circular_buf_list* list;
  int bufferSize;
  worker_list* worker_list;

}server_arg;

int make_connection_worker(int serverPort,const char* serverIP);
circular_buf_list* make_circular_buffer(int num);
void* thread_fun(void* arg);
int  insert_fd_to_buffer(circular_buf_list* list,int fd,int bufferSize,int task);
int network_accept_any(int fds[], unsigned int count,struct sockaddr *addr, socklen_t *addrlen,int * pos);
//int accept_any(int *sockAr, unsigned int limit, struct sockaddr *addr, socklen_t *addrlen, int *opt);
int take_fd(circular_buf_list* list,int bufferSize,int* task);
int take_stats(int readfd);
int read_client(int readfd,worker_list* list,char* worker_ip);
worker_list* make_worker_list();
int insert_to_worker_list(int fd,worker_list* list);
int work_query(int worker_fd,char * buffer,country_list_node* country_list);
int ser_handle_options(int option,int client_fd,int worker_fd,int* count);











#endif
