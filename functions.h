typedef struct date
{
  int Year;
  int Month;
  int Day;
}date;

typedef struct rec_node
{
  int id;
  int age;
  int enter_exit;
  char name[10];
  char last_name[10];
  char diseaseID[20];
  date* date;
  struct rec_node * next;

}rec_node;
typedef struct disease_node
{
  char diseaseID[20];
  struct disease_node * next;

}disease_node;

typedef struct list_stct //for records
{
    rec_node *head;
    rec_node *tail;
}list_stct;

typedef struct disease_list
{
    disease_node *head;
    int disease_count;
    disease_node *tail;
}disease_list;


typedef struct country_parent_node
{
  char country[20];
  int country_num;
  int worker_id;
  struct country_parent_node* next;
}country_parent_node;


typedef struct country_parent_list
{
    country_parent_node *head;
    //country_parent_node *tail;
}country_parent_list;

typedef struct topk_node
{
    int percent;
    int count;
    int name;
    struct topk_node* next;

}topk_node;

typedef struct topk_list
{
    topk_node* head;

}topk_list;


typedef struct country_list_node
{
    struct disease_list*  disease_list;
    struct list_stct* rec_list;
    date ** dates_array ;
    char country[20];
    int country_id;
    int txt_counter;
    struct country_list_node* next;
    rec_node *record_list ;
}country_list_node;


void print_list(country_list_node *head);
country_list_node* insert_to_cntry_list(country_list_node* head,char* country,int country_num);
date* cnvrt_dates(char* buf);
int datecmp(date* date1,date* date2);
void sort_array(date** arr, int n);
void swap(date* xp, date* yp);
void open_txt(char* txt_name,list_stct * list,disease_list* d_list,date* date);
char* make_txt_name(date* date);
void insert_to_list(list_stct*list,rec_node *rec);
int insert_to_disease_list(disease_list* d_list,char* diseaseID);
void insert_dirs_to_list(country_parent_list*list,int first,int last,int id);
int find_enter(int id,list_stct* list);
void print_country(country_parent_list* list,int id);
void get_stats(int readfd,int worker,country_parent_list* country_list);
int option(char* buffer,int ** fd_array,int* pid_array,country_parent_list* country_list,int numWorkers,int requests_count);
void diseaseFrequency(country_parent_list* list ,char* disease,char* cntry,date* date1,date* date2,int numWorkers,int ** fd_array);
void numPatientDischarges1(country_parent_list* list ,char* disease,date* date1,date* date2,int numWorkers,int ** fd_array);
void numPatientDischarges(country_parent_list* list ,char* disease,char* cntry,date* date1,date* date2,int numWorkers,int ** fd_array);
void topkAgeRanges(country_parent_list* list ,char* disease,char* cntry,date* date1,date* date2,int numWorkers,int ** fd_array,int k);
void numPatientAdmissions(country_parent_list* list ,char* disease,char* cntry,date* date1,date* date2,int numWorkers,int ** fd_array);
void numPatientAdmissions1(country_parent_list* list ,char* disease,date* date1,date* date2,int numWorkers,int ** fd_array);
void diseaseFrequency1(country_parent_list* list ,char* disease,date* date1,date* date2,int numWorkers,int ** fd_array);
int worker_wait(int readfd,country_list_node* country_list,int writefd);
int insert_topk_list(topk_list* list,topk_node* new);

void make_logfile(country_list_node* countries,int requests_count);
void make_parent_log(country_parent_list* hand_ptr,int requests_count);
